/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef GOLO_INTERPRETER_H
#define GOLO_INTERPRETER_H

#include <stdio.h>
#include "parser.h"
#include "canvas.h"


/**
 * An interpreter interprets a GOLO program to generate an image.
 */
typedef struct golo_interpreter_t
{
  golo_parser_t parser; /**< Parser to parse the program instructions */
  canvas_t      canvas; /**< Canvas used to draw lines */
} golo_interpreter_t;


void golo_init_interpreter(golo_interpreter_t* interpreter);
int  golo_interpret(golo_interpreter_t* interpreter, FILE* input);
void golo_free_interpreter(golo_interpreter_t* interpreter);


#endif
