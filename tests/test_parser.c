#include <string.h>
#include "test_parser.h"
#include "assert.h"
#include "parser.h"
#include "error.h"


#define golo_assert_id(_id) \
  golo_assert(((golo_ast_node_t*)parser.nodes.last->data)->id == (_id))


static void golo_test_parse_begin_ok()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 50 10") == 0);
  golo_assert(parser.begin->id == GOLO_AST_NODE_BEGIN);
  golo_assert(((golo_begin_t*)parser.begin->data)->width == 50)
  golo_assert(((golo_begin_t*)parser.begin->data)->height == 10)
  
  golo_free_parser(&parser);
}


static void golo_test_parse_begin_fail1()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 50 0") == 1);
  golo_assert(parser.begin->id == GOLO_AST_NODE_BEGIN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_POSITIVE_INTEGER);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_begin_fail2()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 50 50") == 0);
  golo_assert(golo_parse_instr(&parser, "begin 50 50") == 1);
  golo_assert(parser.begin->id == GOLO_AST_NODE_BEGIN);
  golo_assert(golo_error_get().id == GOLO_ERROR_FUNC_INSIDE_BLOC);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_function_ok()
{
  golo_parser_t parser;
  golo_function_t* func;
  golo_variable_t* variable;
  size_t read_size;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func a b c") == 0);
  func = map_get(&parser.functions, "Func", 4, &read_size);
  
  golo_assert_id(GOLO_AST_NODE_FUNCTION);
  golo_assert(func != NULL);
  golo_assert(func->arity == 3);
  
  read_size = 0;
  variable = map_get(&func->variables_map, "a", -1, &read_size);
  golo_assert(variable != NULL);
  golo_assert(read_size == 1);
  
  read_size = 0;
  variable = map_get(&func->variables_map, "b", -1, &read_size);
  golo_assert(variable != NULL);
  golo_assert(read_size == 1);
  
  read_size = 0;
  variable = map_get(&func->variables_map, "c", -1, &read_size);
  golo_assert(variable != NULL);
  golo_assert(read_size == 1);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_function_fail1()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function ") == 1);
  golo_assert_id(GOLO_AST_NODE_FUNCTION);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_IDENTIFIER);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_function_fail2()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function 1") == 1);
  golo_assert_id(GOLO_AST_NODE_FUNCTION);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_IDENTIFIER);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_function_fail3()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function F x") == 0);
  golo_assert(golo_parse_instr(&parser, "function G y") == 1);
  golo_assert_id(GOLO_AST_NODE_FUNCTION);
  golo_assert(golo_error_get().id == GOLO_ERROR_FUNC_INSIDE_BLOC);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_return_ok()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return x") == 0);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(strncmp(expr->token.name, "x", 1) == 0);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_return_fail1()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return") == 1);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_return_fail2()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return x x") == 1);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_alternative_ok(int is_loop)
{
  golo_parser_t parser;
  golo_ast_node_t* bloc;
  golo_alternative_t* alter;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, is_loop ? "while 5 < 4" : "if 5 < 4") == 0);
  golo_assert_id(GOLO_AST_NODE_ALTERNATIVE);
  bloc = parser.nodes.last->data;
  alter = bloc->data;
  golo_assert(alter->is_loop == is_loop);
  golo_assert(parser.blocs.first->data == bloc);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_alternative_fail1(int is_loop)
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser,
                         is_loop ? "while 5 < 4 5" : "if 5 < 4 5") == 1);
  golo_assert_id(GOLO_AST_NODE_ALTERNATIVE);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_alternative_fail2(int is_loop)
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, is_loop ? "while" : "if") == 1);
  golo_assert_id(GOLO_AST_NODE_ALTERNATIVE);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_else_ok(int is_loop)
{
  golo_parser_t parser;
  golo_ast_node_t* bloc;
  golo_alternative_t* alter;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, is_loop ? "while 1" : "if 1") == 0);
  bloc = parser.nodes.last->data;
  alter = bloc->data;
  golo_assert(golo_parse_instr(&parser, "else") == 0);
  golo_assert_id(GOLO_AST_NODE_ELSE);
  golo_assert(alter->alter_node == parser.nodes.last->previous->data);
  golo_assert(alter->else_node == parser.nodes.last->data);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_else_fail1()
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "else") == 1);
  golo_assert_id(GOLO_AST_NODE_ELSE);
  golo_assert(golo_error_get().id == GOLO_ERROR_UNEXPECTED_ELSE);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_else_fail2()
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "if 1") == 0);
  golo_assert(golo_parse_instr(&parser, "else 2") == 1);
  golo_assert_id(GOLO_AST_NODE_ELSE);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_end_ok1(int is_loop)
{
  golo_parser_t parser;
  golo_ast_node_t* bloc;
  golo_alternative_t* alter;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, is_loop ? "while 1" : "if 1") == 0);
  bloc = parser.nodes.last->data;
  alter = bloc->data;
  golo_assert(golo_parse_instr(&parser, "end") == 0);
  golo_assert_id(GOLO_AST_NODE_END);
  golo_assert(alter->end_node == parser.nodes.last->data);
  
  golo_free_parser(&parser);
}


static void golo_test_parse_end_ok2()
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "end") == 0);
  golo_assert_id(GOLO_AST_NODE_END);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_end_fail1()
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "end") == 1);
  golo_assert_id(GOLO_AST_NODE_END);
  golo_assert(golo_error_get().id == GOLO_ERROR_UNEXPECTED_END);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_end_fail2()
{
  golo_parser_t parser;
      
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "end 1") == 1);
  golo_assert_id(GOLO_AST_NODE_END);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_set_ok()
{
  golo_parser_t parser;
  golo_set_instr_t* set;
  golo_begin_t* begin;
  golo_variable_t* variable;
  size_t read_size;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "set x 25") == 0);
  golo_assert_id(GOLO_AST_NODE_SET);
  
  set = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(set->expr->token.value == 25.000f);
  
  begin = parser.begin->data;
  read_size = 0;
  variable = map_get(&begin->function->variables_map, "x", -1, &read_size);
  golo_assert(variable != NULL);
  golo_assert(read_size == 1);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_set_fail1()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "set x 25 22") == 1);
  golo_assert_id(GOLO_AST_NODE_SET);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_set_fail2()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "set ") == 1);
  golo_assert_id(GOLO_AST_NODE_SET);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_IDENTIFIER);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_set_fail3()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "set x") == 1);
  golo_assert_id(GOLO_AST_NODE_SET);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_set_fail4()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "set @x 5") == 1);
  golo_assert_id(GOLO_AST_NODE_SET);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_IDENTIFIER);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_move_turn_ok(int move)
{
  golo_parser_t parser;
  golo_draw_instr_t* instr;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, move ? "move 2" : "turn 2") == 0);
  golo_assert_id(move ? GOLO_AST_NODE_MOVE : GOLO_AST_NODE_TURN);
  
  instr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(instr->exprs[0]->token.value == 2.000f);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_move_turn_fail1(int move)
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, move ? "move" : "turn") == 1);
  golo_assert_id(move ? GOLO_AST_NODE_MOVE : GOLO_AST_NODE_TURN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_move_turn_fail2(int move)
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, move ? "move 1 2" : "turn 1 2") == 1);
  golo_assert_id(move ? GOLO_AST_NODE_MOVE : GOLO_AST_NODE_TURN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_goto_ok()
{
  golo_parser_t parser;
  golo_draw_instr_t* instr;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "goto 1 2") == 0);
  golo_assert_id(GOLO_AST_NODE_GOTO);
  
  instr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(instr->exprs[0]->token.value == 1.000f);
  golo_assert(instr->exprs[1]->token.value == 2.000f);
      
  golo_free_parser(&parser);
}



static void golo_test_parse_goto_fail1()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "goto") == 1);
  golo_assert_id(GOLO_AST_NODE_GOTO);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_goto_fail2()
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "goto 1 2 3") == 1);
  golo_assert_id(GOLO_AST_NODE_GOTO);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_up_down_ok(int up)
{
  golo_parser_t parser;
          
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, up ? "up" : "down") == 0);
  golo_assert_id(up ? GOLO_AST_NODE_UP : GOLO_AST_NODE_DOWN);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_up_down_fail(int up)
{
  golo_parser_t parser;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, up ? "up 1" : "down 2") == 1);
  golo_assert_id(up ? GOLO_AST_NODE_UP : GOLO_AST_NODE_DOWN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_color_ok()
{
  golo_parser_t parser;
  golo_draw_instr_t* instr;
        
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "color 128 50 47") == 0);
  golo_assert_id(GOLO_AST_NODE_COLOR);
  
  instr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(instr->exprs[0]->token.value == 128.000f);
  golo_assert(instr->exprs[1]->token.value == 50.000f);
  golo_assert(instr->exprs[2]->token.value == 47.000f);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_color_fail1()
{
  golo_parser_t parser;
          
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "color 1") == 1);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_color_fail2()
{
  golo_parser_t parser;
          
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "color 1 2") == 1);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EXPR);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_call_ok1()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return Func (x - 1)") == 0);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(strncmp(expr->token.name, "Func", 4) == 0);
  golo_assert(expr->child[0]->token.id == GOLO_TK_MINUS);
  golo_assert(strncmp(expr->child[0]->child[0]->token.name, "x", 1) == 0);
  golo_assert(expr->child[0]->child[1]->token.value == 1.000f);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_call_ok2()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return Func2 (x - 1) y") == 0);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(strncmp(expr->token.name, "Func2", 5) == 0);
  golo_assert(expr->child[0]->token.id == GOLO_TK_MINUS);
  golo_assert(strncmp(expr->child[0]->child[0]->token.name, "x", 1) == 0);
  golo_assert(expr->child[0]->child[1]->token.value == 1.000f);
  golo_assert(strncmp(expr->child[1]->token.name, "y", 1) == 0);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_call_fail()
{
  golo_parser_t parser;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "return Func (x - 1) 3") == 1);
  golo_assert_id(GOLO_AST_NODE_RETURN);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
      
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_ok()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "((5 + x) * 9) -- 7") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_MINUS);
  golo_assert(expr->child[0]->token.id == GOLO_TK_MUL);
  golo_assert(expr->child[1]->token.id == GOLO_TK_UMINUS);
  golo_assert(expr->child[1]->child[0]->token.value == 7.000f);
  golo_assert(expr->child[0]->child[0]->token.id == GOLO_TK_PLUS);
  golo_assert(expr->child[0]->child[0]->child[0]->token.value == 5.000f);
  golo_assert(strncmp(expr->child[0]->child[0]->child[1]->token.name, "x", 1) == 0);
  golo_assert(expr->child[0]->child[1]->token.value == 9.000f);
   
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_fail()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "function Func x") == 0);
  golo_assert(golo_parse_instr(&parser, "8 ** 5") == 1);
  golo_assert_id(GOLO_AST_NODE_EXPR);
     
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_x_y_angle_ok()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "@x - @y - @angle") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_MINUS);
  golo_assert(expr->child[0]->token.id == GOLO_TK_MINUS);
  golo_assert(expr->child[1]->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->child[1]->token.funcid == GOLO_TK_FUNC_ANGLE);
  golo_assert(expr->child[0]->child[0]->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->child[0]->child[0]->token.funcid == GOLO_TK_FUNC_X);
  golo_assert(expr->child[0]->child[1]->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->child[0]->child[1]->token.funcid == GOLO_TK_FUNC_Y);
   
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_width_height_ok()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "@w + @h") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_PLUS);
  golo_assert(expr->child[0]->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->child[0]->token.funcid == GOLO_TK_FUNC_W);
  golo_assert(expr->child[1]->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->child[1]->token.funcid == GOLO_TK_FUNC_H);
   
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_math_ok1()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "Cos 5.5") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->token.funcid == GOLO_TK_FUNC_COS);
  golo_assert(expr->child[0]->token.id == GOLO_TK_NUMBER);
  golo_assert(expr->child[0]->token.value == 5.500f);
   
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_math_ok2()
{
  golo_parser_t parser;
  golo_expr_t* expr;
  
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "Pow 2 8") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_BUILTIN);
  golo_assert(expr->token.funcid == GOLO_TK_FUNC_POW);
  golo_assert(expr->child[0]->token.id == GOLO_TK_NUMBER);
  golo_assert(expr->child[0]->token.value == 2.000f);
  golo_assert(expr->child[1]->token.id == GOLO_TK_NUMBER);
  golo_assert(expr->child[1]->token.value == 8.000f);
     
  golo_free_parser(&parser);
}


static void golo_test_parse_expr_math_fail()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "Cos 5.5 5") == 1);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
   
  golo_free_parser(&parser);
}


static void golo_test_parse_parenthesis_ok()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "1 + (((3 * 4) + 1)) + 4 * 8") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
    
  golo_free_parser(&parser);
}


static void golo_test_parse_parenthesis_fail1()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "1 + ((3 * 4)") == 1);
  golo_assert(golo_error_get().id == GOLO_ERROR_MISSING_RPAR);
     
  golo_free_parser(&parser);
}


static void golo_test_parse_parenthesis_fail2()
{
  golo_parser_t parser;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "1 + 3 * 4)") == 1);
  golo_assert(golo_error_get().id == GOLO_ERROR_EXPECTED_EOL);
     
  golo_free_parser(&parser);
}


static void golo_test_parse_builtin_ok()
{
  golo_parser_t parser;
  golo_expr_t* expr;
    
  golo_init_parser(&parser);
  
  golo_assert(golo_parse_instr(&parser, "begin 1 1") == 0);
  golo_assert(golo_parse_instr(&parser, "(Cos (Cos (25))) * 1000") == 0);
  golo_assert_id(GOLO_AST_NODE_EXPR);
  expr = ((golo_ast_node_t*)parser.nodes.last->data)->data;
  golo_assert(expr->token.id == GOLO_TK_MUL);
     
  golo_free_parser(&parser);
}


void golo_test_parser()
{
  golo_test_parse_begin_ok();
  golo_test_parse_begin_fail1();
  golo_test_parse_begin_fail2();
  golo_test_parse_function_ok();
  golo_test_parse_function_fail1();
  golo_test_parse_function_fail2();
  golo_test_parse_function_fail3();
  golo_test_parse_return_ok();
  golo_test_parse_return_fail1();
  golo_test_parse_return_fail2();
  golo_test_parse_alternative_ok(0);
  golo_test_parse_alternative_fail1(0);
  golo_test_parse_alternative_fail2(0);
  golo_test_parse_alternative_ok(1);
  golo_test_parse_alternative_fail1(1);
  golo_test_parse_alternative_fail2(1);
  golo_test_parse_else_ok(0);
  golo_test_parse_else_ok(1);
  golo_test_parse_else_fail1();
  golo_test_parse_else_fail2();
  golo_test_parse_end_ok1(0);
  golo_test_parse_end_ok1(1);
  golo_test_parse_end_ok2();
  golo_test_parse_end_fail1();
  golo_test_parse_end_fail2();
  golo_test_parse_set_ok();
  golo_test_parse_set_fail1();
  golo_test_parse_set_fail2();
  golo_test_parse_set_fail3();
  golo_test_parse_set_fail4();
  golo_test_parse_move_turn_ok(0);
  golo_test_parse_move_turn_ok(1);
  golo_test_parse_move_turn_fail1(0);
  golo_test_parse_move_turn_fail1(1);
  golo_test_parse_move_turn_fail2(0);
  golo_test_parse_move_turn_fail2(1);
  golo_test_parse_goto_ok();
  golo_test_parse_goto_fail1();
  golo_test_parse_goto_fail2();
  golo_test_parse_up_down_ok(0);
  golo_test_parse_up_down_ok(1);
  golo_test_parse_up_down_fail(0);
  golo_test_parse_up_down_fail(1);
  golo_test_parse_color_ok();
  golo_test_parse_color_fail1();
  golo_test_parse_color_fail2();
  golo_test_parse_call_ok1();
  golo_test_parse_call_ok2();
  golo_test_parse_call_fail();
  golo_test_parse_expr_ok();
  golo_test_parse_expr_fail();
  golo_test_parse_expr_x_y_angle_ok();
  golo_test_parse_expr_width_height_ok();
  golo_test_parse_expr_math_ok1();
  golo_test_parse_expr_math_ok2();
  golo_test_parse_expr_math_fail();
  golo_test_parse_parenthesis_ok();
  golo_test_parse_parenthesis_fail1();
  golo_test_parse_parenthesis_fail2();
  golo_test_parse_builtin_ok();
}
