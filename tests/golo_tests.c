#include "test_list.h"
#include "test_map.h"
#include "test_lexer.h"
#include "test_parser.h"
#include "test_interpreter.h"
#include "test_canvas.h"


int main()
{
  golo_test_list();
  golo_test_map();
  golo_test_lexer();
  golo_test_parser();
  golo_test_interpreter();
  golo_test_canvas();
  
  return 0;
}
