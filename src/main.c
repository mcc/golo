/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <stdio.h>
#include "interpreter.h"


/**
 * @mainpage GOLO Language documentation
 *
 * GOLO is a simple LOGO-like language. See README file for further details.
 */
 
 
int main(int argc, char* argv[]) {
  FILE* finput = argc > 1 ? fopen(argv[1], "r") : stdin;
  FILE* foutput = NULL;
  golo_interpreter_t interpreter;
  
  if (finput != NULL)
  {
    golo_init_interpreter(&interpreter);
    if (golo_interpret(&interpreter, finput) == 0)
    {
      foutput = argc > 2 ? fopen(argv[2], "wb") : stdout;
      canvas_export(&interpreter.canvas, foutput);
    }
    golo_free_interpreter(&interpreter);
  }
  else if (argc > 1)
  {
    fprintf(stderr, "Input file not found : \"%s\".\n", argv[1]);
  }
  
  if (finput != NULL && finput != stdin)
  {
    fclose(finput);
  }
  
  if (foutput != NULL && foutput != stdout)
  {
    fclose(foutput);
  }
  
  return 0;
}
