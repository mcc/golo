#include <string.h>
#include "test_lexer.h"
#include "assert.h"
#include "lexer.h"


static void golo_test_init_lexer()
{
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  golo_assert(lexer.instr == NULL);
  golo_assert(lexer.line == 0);
  golo_assert(lexer.column == 0);
  golo_free_lexer(&lexer);
}


static void golo_test_set_lexer_instr()
{
  char* instr = "TEST";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  golo_assert(lexer.instr == instr);
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token1()
{
  char* instr = "move 12.5(xturnx)";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MOVE);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NUMBER);
  golo_assert(lexer.token.value == 12.5);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_LPAR);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_IDENTIFIER);
  golo_assert(strncmp(lexer.token.name, "xturnx", lexer.token.size) == 0);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_RPAR);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token2()
{
  char* instr = "3*--xmove  -  +7.258)";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NUMBER);
  golo_assert(lexer.token.value == 3);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MUL);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UMINUS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UMINUS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_IDENTIFIER);
  golo_assert(strncmp(lexer.token.name, "xmove", lexer.token.size) == 0);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MINUS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UPLUS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NUMBER);
  golo_assert(lexer.token.value == 7.258000f);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_RPAR);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token3()
{
  char* instr = "upup up";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_IDENTIFIER);
  golo_assert(strncmp(lexer.token.name, "upup", lexer.token.size) == 0);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UP);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token4()
{
  char* instr = "()/*-+%<<=!===>>=||&&!#";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_LPAR);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_RPAR);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_DIV);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MUL);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UMINUS);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UPLUS);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MOD);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_INF);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_INFEQ);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NEQ);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EQ);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_SUP);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_SUPEQ);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_OR);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_AND);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NOT);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token5()
{
  char* instr = "move turn goto up down function begin if else while end\
   set return color";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MOVE);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_TURN);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_GOTO);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_UP);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_DOWN);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_FUNCTION);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BEGIN);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_IF);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_ELSE);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_WHILE);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_END);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_SET);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_RETURN);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_COLOR);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token6()
{
  char* instr = "@x @y @w @h @angle Cos Sin Pow";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_X);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_Y);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_W);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_H);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_ANGLE);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_COS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_SIN);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_POW);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token7()
{
  char* instr = "aa@xbb";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_IDENTIFIER);
  golo_assert(strcmp(lexer.token.name, "aa@xbb") == 0);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


static void golo_test_get_next_token8()
{
  char* instr = "3*@x-@y";
  golo_lexer_t lexer;
  golo_init_lexer(&lexer);
  golo_set_lexer_instr(&lexer, instr);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_NUMBER);
  golo_assert(lexer.token.value == 3.000f);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MUL);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_X);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_MINUS);
  
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_BUILTIN);
  golo_assert(lexer.token.funcid == GOLO_TK_FUNC_Y);
    
  golo_get_next_token(&lexer);
  golo_assert(lexer.token.id == GOLO_TK_EOL);
  
  golo_free_lexer(&lexer);
}


void golo_test_lexer()
{
  golo_test_init_lexer();
  golo_test_set_lexer_instr();
  golo_test_get_next_token1();
  golo_test_get_next_token2();
  golo_test_get_next_token3();
  golo_test_get_next_token4();
  golo_test_get_next_token5();
  golo_test_get_next_token6();
  golo_test_get_next_token7();
  golo_test_get_next_token8();
}
