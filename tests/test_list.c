#include "test_list.h"
#include "assert.h"
#include "list.h"


static void golo_test_list_init()
{
  list_t list;
  
  list_init(&list);
  golo_assert(list.first == NULL);
  golo_assert(list.last == NULL);
}


static void golo_test_list_push_first()
{
  list_t list;
  
  list_init(&list);
  
  list_push_first(&list, (int*)1);
  golo_assert(list.first->data == (int*)1);
  golo_assert(list.first == list.last);
  
  list_push_first(&list, (int*)2);
  golo_assert(list.first->data == (int*)2);
  golo_assert(list.first->next->data == (int*)1);
  golo_assert(list.last->data == (int*)1);
  
  list_free(&list, 0);
}


static void golo_test_list_push_last()
{
  list_t list;
  
  list_init(&list);
  
  list_push_last(&list, (int*)1);
  golo_assert(list.first->data == (int*)1);
  golo_assert(list.first == list.last);
  
  list_push_last(&list, (int*)2);
  golo_assert(list.first->data == (int*)1);
  golo_assert(list.first->next->data == (int*)2);
  golo_assert(list.last->data == (int*)2);
  
  list_free(&list, 0);
}


static void golo_test_list_pop_first()
{
  list_t list;
  void* data;
  
  list_init(&list);
  
  data = list_pop_first(&list);
  golo_assert(data == NULL);
    
  list_push_first(&list, (int*)1);
  list_push_first(&list, (int*)2);
  
  data = list_pop_first(&list);
  golo_assert(data == (int*)2);
  
  data = list_pop_first(&list);
  golo_assert(data == (int*)1);
  
  data = list_pop_first(&list);
  golo_assert(data == NULL);
  
  list_free(&list, 0);
}


static void golo_test_list_pop_last()
{
  list_t list;
  void* data;
  
  list_init(&list);
  
  data = list_pop_last(&list);
  golo_assert(data == NULL);
    
  list_push_last(&list, (int*)1);
  list_push_last(&list, (int*)2);
  
  data = list_pop_last(&list);
  golo_assert(data == (int*)2);
  
  data = list_pop_last(&list);
  golo_assert(data == (int*)1);
  
  data = list_pop_last(&list);
  golo_assert(data == NULL);
  
  list_free(&list, 0);
}


void golo_test_list()
{
  golo_test_list_init();
  golo_test_list_push_first();
  golo_test_list_push_last();
  golo_test_list_pop_first();
  golo_test_list_pop_last();
}
