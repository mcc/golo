# draws a n-gon with a incircle radius of r
# the circle in the polygon is centered at (x,y)
function NGon n r x y
  set oldx @x
  set oldy @y
  set oldangle @angle
  
  set PI 3.14159
  set i 0
  set l 2 * r * (Tan (PI / n)) # edge length
  set a 360 / n                # angle
  
  # we place the turtle at the center of the canvas
  up
  goto x - l / 2 y - r
  down
  
  # we draw the n-gon
  while i < n
    move l
    turn a
    set i i + 1
  end
  
  # we restore the old values of @x @y and @angle
  up
  goto oldx oldy
  turn -@angle + oldangle
  down
end


begin 500 500
  set min 3
  set max 20
  set i min
  
  # we draw some polygons
  while i <= max
    NGon i 100 @w / 2 @h / 2
    set i i + 1
  end
end
