function Fib n
  if n <= 1
    return n 
  end
  return (Fib n - 1) + (Fib n - 2)
end

begin 1 1
  set x Fib 11
  goto x x
end
