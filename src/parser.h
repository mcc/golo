/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef GOLO_PARSER_H
#define GOLO_PARSER_H

#include <stdio.h>
#include "lexer.h"
#include "list.h"


/**
 * List of all possible AST nodes in the GOLO language.
 */
typedef enum golo_ast_node_id_t
{
  GOLO_AST_NODE_EXPR,        /**< expression */
  GOLO_AST_NODE_BEGIN,       /**< begin bloc node */
  GOLO_AST_NODE_FUNCTION,    /**< function bloc node */
  GOLO_AST_NODE_RETURN,      /**< return instruction node */
  GOLO_AST_NODE_ALTERNATIVE, /**< if or while bloc node */
  GOLO_AST_NODE_ELSE,        /**< else bloc node */
  GOLO_AST_NODE_END,         /**< end node */ 
  GOLO_AST_NODE_SET,         /**< set instruction node */
  GOLO_AST_NODE_MOVE,        /**< move instruction node */
  GOLO_AST_NODE_TURN,        /**< turn instruction node */
  GOLO_AST_NODE_GOTO,        /**< goto instruction node */
  GOLO_AST_NODE_UP,          /**< up instruction node */
  GOLO_AST_NODE_DOWN,        /**< down instruction node */
  GOLO_AST_NODE_COLOR        /**< color instruction node */
} golo_ast_node_id_t;


/**
 * Represents an abstract node in the AST. As GOLO is a quite simple langage,
 * each line of a GOLO program can be represented whith one node except for
 * expressions. Concrete nodes are represented by the following structures.
 */
typedef struct golo_ast_node_t
{
  golo_ast_node_id_t      id;   /**< Id of the node */
  void*                   data; /**< Data associated to the node */
  struct golo_ast_node_t* next; /**< Next node in the AST */
} golo_ast_node_t;


/**
 * Represents a variable.
 */
typedef struct golo_variable_t
{
  list_t values;     /**< Values of the variables (it's a list to handle
                       *  recursivity) */
} golo_variable_t;


/**
 * Represents a function bloc, a function call or a begin bloc.
 */
typedef struct golo_function_t
{
  size_t           arity;         /**< Number of formal parameters */
  map_t            variables_map; /**< Associate a variable to name */
  list_t           variables;     /**< List of local variables */
  golo_ast_node_t* func_node;     /**< Function node of the function */
  golo_ast_node_t* end_node;      /**< End node of the function */
} golo_function_t;


/**
 * Represents an expression.
 */
typedef struct golo_expr_t
{
  golo_token_t         token;    /**< Token of the node */
  golo_variable_t*     variable; /**< Node variable if any */
  golo_function_t*     function; /**< Function call if any */
  struct golo_expr_t** child;    /**< Child nodes */
  struct golo_expr_t*  parent;   /**< Parent node */
} golo_expr_t;


/**
 * Represents a if or while bloc.
 */
typedef struct golo_alternative_t
{
  golo_expr_t*     condition;  /**< Expression node of the condition */
  golo_ast_node_t* alter_node; /**< If or while node */
  golo_ast_node_t* else_node;  /**< Else node if any */
  golo_ast_node_t* end_node;   /**< End node of the alternative */
  int              is_loop;    /**< 1 if it's a while node, 0 otherwise */
} golo_alternative_t;


/**
 * Represents a set instruction.
 */
typedef struct golo_set_instr_t
{
  golo_variable_t* variable; /**< Variable of the set instruction */
  golo_expr_t*     expr;     /**< Expression of the set instruction */
} golo_set_instr_t;


/**
 * Represents a move, turn, goto, color, up or down instruction.
 */
typedef struct golo_draw_instr_t
{
  size_t        arity; /**< Number of expressions */
  golo_expr_t** exprs; /**< Child expressions */
} golo_draw_instr_t;


/**
 * Represents a begin bloc.
 */
typedef struct golo_begin_t
{
  int              width;    /**< Width of the canvas */
  int              height;   /**< Height of the canvas */
  golo_function_t* function; /**< Main function */
} golo_begin_t;


/**
 * A parser analyses the tokens to generate an abstract syntaxic tree (AST).
 */
typedef struct golo_parser_t
{
  golo_lexer_t     lexer;     /**< Lexer to generate tokens */
  list_t           nodes;     /**< Nodes of the AST */
  list_t           blocs;     /**< Stack of encountered blocs */
  map_t            functions; /**< Map associating names to functions */
  golo_ast_node_t* begin;     /**< Begin node */
} golo_parser_t;


void golo_init_parser(golo_parser_t* parser);
int  golo_parse_instr(golo_parser_t* parser, char* instr);
int  golo_parse(golo_parser_t* parser, FILE* input);
void golo_free_parser(golo_parser_t* parser);


#endif
