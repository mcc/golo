/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file list.c
 * 
 * This file provides functions to add and remove elements in a double-linked
 * list.
 */
 
 
#include <stdlib.h>
#include "list.h"


/**
 * Initializes a list.
 *
 * @param list list instance to initialize
 */
void list_init(list_t* list)
{
  list->first = list->last = NULL;
}


/**
 * Adds an element at the beginning of the list.
 *
 * @param list list instance to use
 * @param data data to add
 */
void list_push_first(list_t* list, void* data)
{
  list_element_t* current = malloc(sizeof(list_element_t));
  
  current->data = data;
  current->next = list->first;
  current->previous = NULL;
  if (list->first != NULL)
  {
    list->first->previous = current;
  }
  list->first = current;
  if (list->last == NULL)
  {
    list->last = current;
  }
}


/**
 * Adds an element at the end of the list.
 *
 * @param list list instance to use
 * @param data data to add
 */
void list_push_last(list_t* list, void* data)
{
  list_element_t* current = malloc(sizeof(list_element_t));
  
  current->data = data;
  current->next = NULL;
  current->previous = list->last;
  if (list->last != NULL)
  {
    list->last->next = current;
  }
  list->last = current;
  if (list->first == NULL)
  {
    list->first = current;
  }
}


/**
 * Removes and returns the first element of the list.
 *
 * @param list list instance to use
 * @returns the data of the first element of the list or NULL if the list is
 *          empty
 */
void* list_pop_first(list_t* list)
{
  list_element_t* first = list->first;
  void* data = first == NULL ? NULL : first->data;
  
  if (list->first != NULL)
  {
    list->first = first->next;
    free(first);
    if (list->first == NULL)
    {
      list->last = NULL;
    }
    else
    {
      list->first->previous = NULL;
    }
  }
  
  return data;
}


/**
 * Removes and returns the last element of the list.
 *
 * @param list list instance to use
 * @returns the data of the last element of the list or NULL if the list is
 *          empty
 */
void* list_pop_last(list_t* list)
{
  list_element_t* last = list->last;
  void* data = last == NULL ? NULL : last->data;
  
  if (last != NULL)
  {
    list->last = last->previous;
    free(last);
    if (list->last == NULL)
    {
      list->first = NULL;
    }
    else
    {
      list->last->next = NULL;
    }
  }
  
  return data;
}


/**
 * Frees the list.
 *
 * @param list list instance to free
 * @param free_data if its value is 1, elements data are freed, otherwise only
 *        wrapper are freed
 */
void list_free(list_t* list, int free_data)
{
  list_element_t* current = list->first;
  list_element_t* tmp;
  
  while (current != NULL)
  {
    tmp = current;
    if (tmp->data != NULL && free_data)
    {
      free(tmp->data);
    }
    current = current->next;
    free(tmp);
  }
  list->first = list->last = NULL;
}
