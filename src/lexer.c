/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file lexer.c
 * 
 * This file provides functions to cut a string into tokens defined in the GOLO
 * languages.
 */
 

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "lexer.h"


/**
 * Adds a token in the tokens map. We associate an id to each token in a map.
 *
 * @param lexer lexer which contains the map of tokens
 * @param str string representation of the token
 * @param id id of the token
 */
static void golo_map_token(golo_lexer_t* lexer, char* str, golo_token_id_t id)
{
  golo_token_id_t* pid = malloc(sizeof(golo_token_id_t));
  *pid = id;
  map_set(&lexer->tokens, str, -1, pid);
}


/**
 * Adds a function in the functions map. We associate a function id to each
 * built-in function in a map.
 *
 * @param lexer lexer which contains the map of functions
 * @param str name of the function
 * @param funcid id of the function
 */
static void golo_map_function(golo_lexer_t* lexer, char* str,
                              golo_token_func_id_t funcid)
{
  golo_token_func_id_t* pfuncid = malloc(sizeof(golo_token_func_id_t));
  golo_token_id_t* pid = malloc(sizeof(golo_token_id_t));
  *pfuncid = funcid;
  *pid = GOLO_TK_BUILTIN;
  map_set(&lexer->functions, str, -1, pfuncid);
  map_set(&lexer->tokens, str, -1, pid);
}


/**
 * Initialize a lexer.
 *
 * @param lexer lexer to initialize
 */
void golo_init_lexer(golo_lexer_t* lexer)
{
  map_init(&lexer->tokens);
  golo_map_token(lexer, "move",     GOLO_TK_MOVE);
  golo_map_token(lexer, "turn",     GOLO_TK_TURN);
  golo_map_token(lexer, "goto",     GOLO_TK_GOTO);
  golo_map_token(lexer, "up",       GOLO_TK_UP);
  golo_map_token(lexer, "down",     GOLO_TK_DOWN);
  golo_map_token(lexer, "color",    GOLO_TK_COLOR);
  golo_map_token(lexer, "set",      GOLO_TK_SET);
  golo_map_token(lexer, "while",    GOLO_TK_WHILE);
  golo_map_token(lexer, "if",       GOLO_TK_IF);
  golo_map_token(lexer, "else",     GOLO_TK_ELSE);
  golo_map_token(lexer, "end",      GOLO_TK_END);
  golo_map_token(lexer, "!",        GOLO_TK_NOT);
  golo_map_token(lexer, "&&",       GOLO_TK_AND);
  golo_map_token(lexer, "||",       GOLO_TK_OR);
  golo_map_token(lexer, "==",       GOLO_TK_EQ);
  golo_map_token(lexer, "!=",       GOLO_TK_NEQ);
  golo_map_token(lexer, "<",        GOLO_TK_INF);
  golo_map_token(lexer, "<=",       GOLO_TK_INFEQ);
  golo_map_token(lexer, ">",        GOLO_TK_SUP);
  golo_map_token(lexer, ">=",       GOLO_TK_SUPEQ);
  golo_map_token(lexer, "*",        GOLO_TK_MUL);
  golo_map_token(lexer, "/",        GOLO_TK_DIV);
  golo_map_token(lexer, "%",        GOLO_TK_MOD);
  golo_map_token(lexer, "+",        GOLO_TK_PLUS);
  golo_map_token(lexer, "-",        GOLO_TK_MINUS);
  golo_map_token(lexer, "(",        GOLO_TK_LPAR);
  golo_map_token(lexer, ")",        GOLO_TK_RPAR);
  golo_map_token(lexer, "function", GOLO_TK_FUNCTION);
  golo_map_token(lexer, "return",   GOLO_TK_RETURN);
  golo_map_token(lexer, "begin",    GOLO_TK_BEGIN);
  golo_map_token(lexer, "#",        GOLO_TK_EOL);
  
  map_init(&lexer->functions);
  golo_map_function(lexer, "@x",     GOLO_TK_FUNC_X);
  golo_map_function(lexer, "@y",     GOLO_TK_FUNC_Y);
  golo_map_function(lexer, "@w",     GOLO_TK_FUNC_W);
  golo_map_function(lexer, "@h",     GOLO_TK_FUNC_H);
  golo_map_function(lexer, "@r",     GOLO_TK_FUNC_R);
  golo_map_function(lexer, "@g",     GOLO_TK_FUNC_G);
  golo_map_function(lexer, "@b",     GOLO_TK_FUNC_B);
  golo_map_function(lexer, "@angle", GOLO_TK_FUNC_ANGLE);
  golo_map_function(lexer, "Cos",    GOLO_TK_FUNC_COS);
  golo_map_function(lexer, "Sin",    GOLO_TK_FUNC_SIN);
  golo_map_function(lexer, "Tan",    GOLO_TK_FUNC_TAN);
  golo_map_function(lexer, "Acos",   GOLO_TK_FUNC_ACOS);
  golo_map_function(lexer, "Asin",   GOLO_TK_FUNC_ASIN);
  golo_map_function(lexer, "Atan",   GOLO_TK_FUNC_ATAN);
  golo_map_function(lexer, "Atan2",  GOLO_TK_FUNC_ATAN2);
  golo_map_function(lexer, "Pow",    GOLO_TK_FUNC_POW);
  golo_map_function(lexer, "Sqrt",   GOLO_TK_FUNC_SQRT);
  golo_map_function(lexer, "Log",    GOLO_TK_FUNC_LOG);
  golo_map_function(lexer, "Log10",  GOLO_TK_FUNC_LOG10);
  
  golo_set_lexer_instr(lexer, NULL);
  lexer->line = 0;
}


/**
 * Specifies the instruction to tokenize.
 *
 * @param lexer lexer to initialize
 * @param instr instruction string to tokenize
 */
void golo_set_lexer_instr(golo_lexer_t* lexer, char* instr)
{
  lexer->last_read_token_id = GOLO_TK_EOL;
  lexer->token.id = GOLO_TK_EOL;
  lexer->instr = instr;
  lexer->line++;
  lexer->column = 0;
}


/**
 * Return the priority of a token. It is usefull to parse expressions.
 * 
 *
 * @param id id of the token
 * @returns the priority of the token
 */
static int golo_get_token_priority(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_NUMBER:
    case GOLO_TK_BUILTIN:
    case GOLO_TK_IDENTIFIER: return 0;
    case GOLO_TK_UMINUS:
    case GOLO_TK_UPLUS:
    case GOLO_TK_NOT:        return 1;
    case GOLO_TK_MUL:
    case GOLO_TK_DIV:
    case GOLO_TK_MOD:        return 2;
    case GOLO_TK_PLUS:
    case GOLO_TK_MINUS:      return 3;
    case GOLO_TK_INF:
    case GOLO_TK_INFEQ:
    case GOLO_TK_SUP:
    case GOLO_TK_SUPEQ:      return 4;
    case GOLO_TK_EQ:
    case GOLO_TK_NEQ:        return 5;
    case GOLO_TK_AND:        return 6;
    case GOLO_TK_OR:         return 7;
    default:                 return -1;
  }
}


/**
 * Returns the arity of a token. It is usefull to parse expressions.
 *
 * @param id id of the token
 * @param funcid id of the function
 * @returns the arity of the token
 */
static int golo_get_token_arity(golo_token_id_t id, golo_token_func_id_t funcid)
{
  if (id == GOLO_TK_BUILTIN)
  {
    switch (funcid)
    {
      case GOLO_TK_FUNC_X:
      case GOLO_TK_FUNC_Y:
      case GOLO_TK_FUNC_W:
      case GOLO_TK_FUNC_H:
      case GOLO_TK_FUNC_R:
      case GOLO_TK_FUNC_G:
      case GOLO_TK_FUNC_B:
      case GOLO_TK_FUNC_NONE:
      case GOLO_TK_FUNC_ANGLE: return 0;
      case GOLO_TK_FUNC_COS:
      case GOLO_TK_FUNC_SIN:
      case GOLO_TK_FUNC_TAN:
      case GOLO_TK_FUNC_ACOS:
      case GOLO_TK_FUNC_ASIN:
      case GOLO_TK_FUNC_ATAN:
      case GOLO_TK_FUNC_SQRT:
      case GOLO_TK_FUNC_LOG:
      case GOLO_TK_FUNC_LOG10: return 1;
      case GOLO_TK_FUNC_POW:
      case GOLO_TK_FUNC_ATAN2: return 2;
    }
  }
  else
  {
    switch (id)
    {
      default:
      case GOLO_TK_UP:
      case GOLO_TK_DOWN:
      case GOLO_TK_NUMBER:
      case GOLO_TK_IDENTIFIER: return 0;
      case GOLO_TK_UMINUS:
      case GOLO_TK_UPLUS:
      case GOLO_TK_MOVE:
      case GOLO_TK_TURN:
      case GOLO_TK_NOT:        return 1;
      case GOLO_TK_GOTO:
      case GOLO_TK_MUL:
      case GOLO_TK_DIV:
      case GOLO_TK_MOD:
      case GOLO_TK_PLUS:
      case GOLO_TK_MINUS:
      case GOLO_TK_INF:
      case GOLO_TK_INFEQ:
      case GOLO_TK_SUP:
      case GOLO_TK_SUPEQ:
      case GOLO_TK_EQ:
      case GOLO_TK_NEQ:
      case GOLO_TK_AND:
      case GOLO_TK_OR:         return 2;
      case GOLO_TK_COLOR:      return 3;
    }
  }
  
  return 0;
}


/**
 * Indicates if the token is alphanumeric.
 *
 * @param id id of the token
 * @returns 1 if the token is alphanumeric, 0 otherwise
 */
static int golo_token_isalnum(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_EOL:
    case GOLO_TK_LPAR:
    case GOLO_TK_RPAR:
    case GOLO_TK_NUMBER:
    case GOLO_TK_UMINUS:
    case GOLO_TK_UPLUS:
    case GOLO_TK_NOT:
    case GOLO_TK_MUL:
    case GOLO_TK_DIV:
    case GOLO_TK_MOD:
    case GOLO_TK_PLUS:
    case GOLO_TK_MINUS:
    case GOLO_TK_INF:
    case GOLO_TK_INFEQ:
    case GOLO_TK_SUP:
    case GOLO_TK_SUPEQ:
    case GOLO_TK_EQ:
    case GOLO_TK_NEQ:
    case GOLO_TK_AND:
    case GOLO_TK_OR: return 0;
    default:         return 1;
  }
}


/**
 * Indicates if a token can be inside an instruction bloc.
 *
 * @param id id of the token
 * @returns 1 if the token can be inside a bloc, 0 otherwise
 */
int golo_is_token_inside_bloc(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_BEGIN:
    case GOLO_TK_FUNCTION: return 0;
    default:               return 1;
  }
}


/**
 * Indicates if a token can be outside an instruction bloc.
 *
 * @param id id of the token
 * @returns 1 if the token can be outside a bloc, 0 otherwise
 */
int golo_is_token_outside_bloc(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_END:
    case GOLO_TK_BEGIN:
    case GOLO_TK_FUNCTION: return 1;
    default:               return 0;
  }
}


/**
 * Indicates if a token is an identifier, a number or a left parenthesis.
 *
 * @param id id of the token
 * @returns 1 if the token is an identifier, a number or a left parenthesis, 0
 *          otherwise
 */
int golo_token_is_value(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_LPAR: 
    case GOLO_TK_NUMBER:
    case GOLO_TK_BUILTIN:
    case GOLO_TK_IDENTIFIER: return 1;
    default:                 return 0;
  }
}


/**
 * Indicates if a token is an identifier, a number or a right parenthesis.
 *
 * @param id id of the token
 * @returns 1 if the token is an identifier, a number or a right parenthesis, 0
 *          otherwise
 */
static int golo_token_was_value(golo_token_id_t id)
{
  switch (id)
  {
    case GOLO_TK_RPAR: 
    case GOLO_TK_NUMBER:
    case GOLO_TK_BUILTIN:
    case GOLO_TK_IDENTIFIER: return 1;
    default:                 return 0;
  }
}


/**
 * Initializes a token.
 *
 * @param lexer lexer instance to use
 * @param token token to initialize
 */
void golo_init_token(golo_lexer_t* lexer, golo_token_t* token)
{
  token->id = GOLO_TK_EOL;
  token->funcid = GOLO_TK_FUNC_NONE;
  token->name = NULL;
  token->value = 0;
  token->size = 0;
  token->arity = 0;
  token->priority = -1;
  token->column = lexer->column;
  token->line = lexer->line;
}


/**
 * Read the last token.
 *
 * @param lexer lexer instance to use
 * @returns the size of the read token
 */
int golo_get_next_token(golo_lexer_t* lexer)
{
  char* str = NULL;
  size_t offset;
  size_t read_size = 0;
  size_t read_size2 = 0;
  golo_token_id_t* pid;
  golo_token_func_id_t* pfuncid;
  
  lexer->last_read_token_id = lexer->token.id;
  golo_init_token(lexer, &lexer->token);
    
  // We search the start and the end of the next token.
  while (isspace(*lexer->instr) && *lexer->instr != '\0')
  {
    lexer->instr++;
    lexer->column++;
  }
  while (!isspace(lexer->instr[lexer->token.size]) &&
         lexer->instr[lexer->token.size] != '\0')
  {
    lexer->token.size++;
  }
    
  if (lexer->token.size > 0)
  {
    // We check if the read token correspond to a predefined token.
    pid = (golo_token_id_t*)map_get(&lexer->tokens, lexer->instr,
                                    lexer->token.size, &read_size);
    if (pid != NULL && lexer->token.size != read_size &&
        golo_token_isalnum(*pid) && isalnum(lexer->instr[read_size]))
    {
      // If the string starts with a predefined token but this token cannot be
      // followed by digits or letters, the string will be considered as an
      // identifier. For example "MOVE123" cannot be considered as the token
      // MOVE followed by 123 because we want to be able to use identifiers
      // which start with predefined token names.
      pid = NULL;
    }
    
    if (pid != NULL)
    {
      lexer->token.id = *pid;
      
      if (*pid == GOLO_TK_BUILTIN)
      {
        // We get the id of the built-in function.
        read_size2 = 0;
        pfuncid = (golo_token_func_id_t*)map_get(&lexer->functions,
                                                 lexer->instr,
                                                 read_size,
                                                 &read_size2);
        if (pfuncid != NULL && read_size2 == read_size)
        {
          lexer->token.funcid = *pfuncid;
        }
        else
        {
          lexer->token.funcid = GOLO_TK_FUNC_NONE;
        }
      }
      else
      {
        // We have to handle the case where a - or a + is read after a non value
        // token. In this case the operator is unary, for exemple in "2 * -5" - is
        // unary but in "2 - 5" - is binary.
        if (!golo_token_was_value(lexer->last_read_token_id))
        {
          if (lexer->token.id == GOLO_TK_PLUS)
          {
            lexer->token.id = GOLO_TK_UPLUS;
          }
          if (lexer->token.id == GOLO_TK_MINUS)
          {
            lexer->token.id = GOLO_TK_UMINUS;
          }
        }
      }
      lexer->token.size = read_size;
    }
    else
    {
      // We check if the token starts with a number. In this case strtod
      // modifies str by the address pointing after the end of the read number.
      // So if we make a pointer difference, we can retrieve the size of the
      // number.
      lexer->token.value = strtod(lexer->instr, &str);
      if (lexer->instr != str)
      {
        lexer->token.id = GOLO_TK_NUMBER;
        lexer->token.size = str - lexer->instr;
      }
      else
      {
        // The read token is an identifier. However the token has to be analysed
        // to handle the case where it contains a predefined token which can be
        // preceded by letters or digits. For example, if we have read "abc*4",
        // the identifier is "abc" and not "abc*4". To treat this case, we try
        // to detect a predefined token in the whole string. In the example, we
        // check "bc*4", "c*4" and "*4". "*" is a predefined token which can be
        // preceded by a letter so it cannot be part of the identifier.
        // However, if the string is "abcMOVE" the identifier is the whole
        // string because the predefined token MOVE cannot be preceded or
        // followed by letters and digits.
        lexer->token.value = 0;
        pid = NULL;
        for (offset = 1; pid == NULL && offset < lexer->token.size;
             offset++)
        {
          read_size = 0;
          pid = (golo_token_id_t*)map_get(&lexer->tokens,
                                          lexer->instr + offset,
                                          lexer->token.size - offset,
                                          &read_size);
          if (pid != NULL && golo_token_isalnum(*pid))
          {
            pid = NULL;
          }
        }
        if (pid != NULL)
        {
          offset--;
        }
        
        lexer->token.id = GOLO_TK_IDENTIFIER;
        lexer->token.size = offset;
        lexer->token.name = lexer->instr;
      }
    }
  }
  
  lexer->token.priority = golo_get_token_priority(lexer->token.id);
  lexer->token.arity = golo_get_token_arity(lexer->token.id,
                                            lexer->token.funcid);
  lexer->instr += lexer->token.size;
  lexer->token.column = lexer->column;
  lexer->column += lexer->token.size;
    
  return lexer->token.size;
}


/**
 * Frees the given lexer.
 *
 * @param lexer lexer instance to free
 */
void golo_free_lexer(golo_lexer_t* lexer)
{
  map_free(&lexer->tokens, 1);
  map_free(&lexer->functions, 1);
}
