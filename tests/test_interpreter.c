#include "test_interpreter.h"
#include "assert.h"
#include "error.h"
#include "interpreter.h"


static void golo_test_interpreter_init()
{
  golo_interpreter_t interpreter;
  
  golo_init_interpreter(&interpreter);
  golo_assert(interpreter.canvas.width == 0);
  golo_assert(interpreter.canvas.height == 0);
  golo_assert(interpreter.canvas.x == 0);
  golo_assert(interpreter.canvas.y == 0);
  golo_assert(interpreter.canvas.angle == 0);
  golo_assert(interpreter.canvas.isdown == 1);
  golo_assert(interpreter.canvas.color.r == 0);
  golo_assert(interpreter.canvas.color.g == 0);
  golo_assert(interpreter.canvas.color.b == 0);
  golo_free_interpreter(&interpreter);
}


static void golo_test_interpreter_with_file(char* filename, float x, float y)
{
  golo_interpreter_t interpreter;
  FILE* finput = fopen(filename, "r");
  
  golo_assert(finput != NULL);
  if (finput != NULL)
  {
    golo_init_interpreter(&interpreter);
    golo_assert(golo_interpret(&interpreter, finput) == 0);
    golo_assert(interpreter.canvas.x == x);
    golo_assert(interpreter.canvas.y == y);
    golo_free_interpreter(&interpreter);
    fclose(finput);
  }
}


static void golo_test_interpreter_unknown_identifier()
{
  golo_interpreter_t interpreter;
  FILE* finput = fopen("tests/golo/unknown_identifier.golo", "r");
  
  golo_assert(finput != NULL);
  if (finput != NULL)
  {
    golo_init_interpreter(&interpreter);
    golo_assert(golo_interpret(&interpreter, finput) == 1);
    golo_assert(golo_error_get().id == GOLO_ERROR_UNKNOWN_IDENTIFIER);
    golo_free_interpreter(&interpreter);
    fclose(finput);
  }
}


static void golo_test_interpreter_unknown_function()
{
  golo_interpreter_t interpreter;
  FILE* finput = fopen("tests/golo/unknown_function.golo", "r");
  
  golo_assert(finput != NULL);
  if (finput != NULL)
  {
    golo_init_interpreter(&interpreter);
    golo_assert(golo_interpret(&interpreter, finput) == 1);
    golo_assert(golo_error_get().id == GOLO_ERROR_UNKNOWN_FUNCTION);
    golo_free_interpreter(&interpreter);
    fclose(finput);
  }
}


void golo_test_interpreter()
{
  golo_test_interpreter_init();
  golo_test_interpreter_with_file("tests/golo/exp.golo", 120, 120);
  golo_test_interpreter_with_file("tests/golo/fib.golo", 89, 89);
  golo_test_interpreter_with_file("tests/golo/while.golo", 6, 25);
  golo_test_interpreter_with_file("tests/golo/trigo.golo", 0, 0);
  golo_test_interpreter_unknown_identifier();
  golo_test_interpreter_unknown_function();
}
