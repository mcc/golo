function Exp n
  if n == 0
    return 1
  end
  return n * (Exp n - 1)
end

begin 1 1
  set x (Exp 5)
  goto x x
end
