/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef MAP_H
#define MAP_H


/**
 * map_t is a tree map which associate string to values. Each map_t element is
 * a node in the tree.
 */
typedef struct map_t
{
  char          label; /**< Character associated to the node */
  void*         data;  /**< Data associated to the node */
  struct map_t* child; /**< Child nodes */
  struct map_t* next;  /**< Sibling node */
} map_t;


void  map_init(map_t* map);
void* map_set(map_t* map, const char* str, int size, void* data);
void* map_get(map_t* map, const char* str, int size, size_t* read_size);
void  map_free(map_t* map, int free_data);


#endif
