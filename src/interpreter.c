/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file interpreter.c
 * 
 * This file provides functions to interpret the AST nodes produced by the
 * parser.
 */


#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "interpreter.h"
#include "error.h"


#define UPSILON 0.0000001


static int golo_interpret_bloc(golo_interpreter_t* interpreter,
                               golo_ast_node_t* first,
                               golo_ast_node_t* last,
                               int* returned,
                               float* retvalue);


static int golo_interpret_expr(golo_interpreter_t* interpreter,
                               golo_expr_t* expr, float* result);



/**
 * Initializes the interpreter.
 *
 * @param interpreter interpreter to initialize
 */
void golo_init_interpreter(golo_interpreter_t* interpreter)
{
  golo_init_parser(&interpreter->parser);
  canvas_init(&interpreter->canvas, 0, 0);
}


/**
 * Interprets a begin node.
 *
 * @param interpreter interpreter instance to use
 * @param begin begin node to interpret
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_begin(golo_interpreter_t* interpreter,
                                golo_begin_t* begin)
{
  canvas_init(&interpreter->canvas, begin->width, begin->height);
  
  return 0;
}


/**
 * Interprets a boolean && operator.
 *
 * @param interpreter interpreter instance to use
 * @param expr && operator node
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_expr_and(golo_interpreter_t* interpreter,
                                   golo_expr_t* expr, float* result)
{
  float tmp = 0;
  int ret = 0;
  
  *result = 0;
  ret = golo_interpret_expr(interpreter, expr->child[0], &tmp);
  *result = tmp != 0;
  
  if (ret == 0 && *result)
  {
    ret = golo_interpret_expr(interpreter, expr->child[1], &tmp);
    *result = *result && tmp != 0;
  }

  return ret;
}


/**
 * Interprets a boolean || operator.
 *
 * @param interpreter interpreter instance to use
 * @param expr || operator node
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_expr_or(golo_interpreter_t* interpreter,
                                  golo_expr_t* expr, float* result)
{
  float tmp = 0;
  int ret = 0;
  
  *result = 0;
  ret = golo_interpret_expr(interpreter, expr->child[0], &tmp);
  *result = tmp != 0;
  
  if (ret == 0 && !*result)
  {
    ret = golo_interpret_expr(interpreter, expr->child[1], &tmp);
    *result = *result || tmp != 0;
  }

  return ret;
}


/**
 * Interprets a boolean ! operator, a unary - or a unary +.
 *
 * @param interpreter interpreter instance to use
 * @param expr node to interpret
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_expr_unary(golo_interpreter_t* interpreter,
                                     golo_expr_t* expr, float* result)
{
  int ret = 0;
  
  *result = 0;
  
  ret = golo_interpret_expr(interpreter, expr->child[0], result);
  if (ret == 0)
  {
    switch (expr->token.id)
    {
      default:
      case GOLO_TK_UPLUS:
        break;
      case GOLO_TK_UMINUS:
        *result = -*result;
        break;
      case GOLO_TK_NOT:
        *result = !*result;
        break;
    }
  }
  
  return ret;
}


/**
 * Interprets a binary operator.
 *
 * @param interpreter interpreter instance to use
 * @param expr binary operator node to interpret
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_expr_binary(golo_interpreter_t* interpreter,
                                      golo_expr_t* expr, float* result)
{
  float left = 0;
  float right = 0;
  int ret = 0;
  
  *result = 0;

  ret = golo_interpret_expr(interpreter, expr->child[0], &left);
  if (ret == 0)
  {
    ret = golo_interpret_expr(interpreter, expr->child[1], &right);
    if (ret == 0)
    {
      switch (expr->token.id)
      {
        case GOLO_TK_MUL:
          *result = left * right;
          break;
        case GOLO_TK_DIV:
          if (right == 0)
          {
            golo_error_set(GOLO_ERROR_DIVISION_BY_ZERO, expr->token.line,
              expr->token.column);
            ret = 1;
          }
          else
          {
            *result = left / right;
          }
          break;
        case GOLO_TK_MOD:
          if (right == 0)
          {
            ret = 1;
          }
          else
          {
            *result = left / right;
          }
          break;
        case GOLO_TK_PLUS:
          *result = left + right;
          break;
        case GOLO_TK_MINUS:
          *result = left - right;
          break;
        case GOLO_TK_INF:
          *result = left < right;
          break;
        case GOLO_TK_INFEQ:
          *result = left <= right;
          break;
        case GOLO_TK_SUP:
          *result = left > right;
          break;
        case GOLO_TK_SUPEQ:
          *result = left >= right;
          break;
        case GOLO_TK_EQ:
          *result = left == right;
          break;
        case GOLO_TK_NEQ:
          *result = fabs(left - right) >= UPSILON;
          break;
        default:
          break;
      }
    }
  }
  
  return ret;
}


/**
 * Interprets an identifier node (variable or function call).
 *
 * @param interpreter interpreter instance to use
 * @param expr identifier to interpret
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_identifier(golo_interpreter_t* interpreter,
                                     golo_expr_t* expr,
                                     float* result)
{
  int ret = 0;
  size_t n = 0;
  float fparam;
  float* pvalue;
  list_element_t* elem;
  golo_variable_t* variable;
  int returned = 0;
  
  *result = 0;
  if (expr->variable != NULL)
  {
    // If the identifier is a variable, we return its value.
    elem = expr->variable->values.first;
    if (elem != NULL && elem->data != NULL)
    {
      *result = *((float*)elem->data);
    }
  }
  else if (expr->function != NULL && expr->function->func_node != NULL)
  {
    // It's a function call. We check the arity, we interpret the parameters
    // and we execute the function.
    if (expr->function->arity != expr->token.arity)
    {
      golo_error_set(GOLO_ERROR_UNKNOWN_FUNCTION, expr->token.line,
        expr->token.column);
      ret = 1;
    }
    else
    {
      // We initialize all local variables and parameters.
      for (elem = expr->function->variables.first; elem != NULL;
           elem = elem->next)
      {
        variable = elem->data;
        pvalue = malloc(sizeof(float));
        if (variable->values.first != NULL &&
            variable->values.first->data != NULL)
        {
          *pvalue = *((float*)variable->values.first->data);
        }
        else
        {
          *pvalue = 0;
        }
        list_push_first(&variable->values, pvalue);
      }
      
      // The first variables correspond to the function's parameters.
      for (elem = expr->function->variables.first, n = 0;
           elem != NULL && ret == 0 && n < expr->function->arity;
           n++)
      {
        ret = golo_interpret_expr(interpreter, expr->child[n], &fparam);
        if (ret == 0)
        {
          // We assign the interpreted value to the corresponding parameter.
          variable = elem->data;
          pvalue = variable->values.first->data;
          *pvalue = fparam;
          elem = elem->next;
        }
      }
      
      if (ret == 0)
      {
        // We can call the function.
        ret = golo_interpret_bloc(interpreter, expr->function->func_node->next,
          expr->function->end_node, &returned, result);
      }
        
      // We free the local variable values.
      for (elem = expr->function->variables.first; elem != NULL;
           elem = elem->next)
      {
        variable = elem->data;
        pvalue = list_pop_first(&variable->values);
        if (pvalue != NULL)
        {
          free(pvalue);
        }
      }
    }
  }
  else
  {
    golo_error_set(GOLO_ERROR_UNKNOWN_IDENTIFIER, expr->token.line,
      expr->token.column);
    ret = 1;
  }
  
  return ret;
}


/**
 * Interprets a built-in function node.
 *
 * @param interpreter interpreter instance to use
 * @param builtin built-in function node to interpret
 * @param result value modified by the result of the interpretation
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_builtin(golo_interpreter_t* interpreter,
                                  golo_expr_t* builtin, float* result)
{
  int ret = 0;
  float* results = NULL;
  size_t n;
  
  if (builtin->token.arity > 0)
  {
    results = malloc(sizeof(float) * builtin->token.arity);
    for (n = 0; ret == 0 && n < builtin->token.arity; n++)
    {
      results[n] = 0;
      ret = golo_interpret_expr(interpreter, builtin->child[n], &results[n]);
    }
  }
  
  *result = 0;
  
  switch (builtin->token.funcid)
  {
    case GOLO_TK_FUNC_X:
      *result = interpreter->canvas.x;
      break;
    case GOLO_TK_FUNC_Y:
      *result = interpreter->canvas.y;
      break;
    case GOLO_TK_FUNC_ANGLE:
      *result = interpreter->canvas.angle;
      break;
    case GOLO_TK_FUNC_W:
      *result = interpreter->canvas.width;
      break;
    case GOLO_TK_FUNC_H:
      *result = interpreter->canvas.height;
      break;
    case GOLO_TK_FUNC_R:
      *result = (float)interpreter->canvas.pixels
                  [(int)round(interpreter->canvas.y)]
                  [(int)round(interpreter->canvas.x)].r;
      break;
    case GOLO_TK_FUNC_G:
      *result = (float)interpreter->canvas.pixels
                  [(int)round(interpreter->canvas.y)]
                  [(int)round(interpreter->canvas.x)].g;
      break;
    case GOLO_TK_FUNC_B:
      *result = (float)interpreter->canvas.pixels
                  [(int)round(interpreter->canvas.y)]
                  [(int)round(interpreter->canvas.x)].b;
      break;
    case GOLO_TK_FUNC_COS:
      *result = cos(results[0]);
      break;
    case GOLO_TK_FUNC_SIN:
      *result = sin(results[0]);
      break;
    case GOLO_TK_FUNC_TAN:
      *result = tan(results[0]);
      break;
    case GOLO_TK_FUNC_ACOS:
      *result = acos(results[0]);
      break;
    case GOLO_TK_FUNC_ASIN:
      *result = asin(results[0]);
      break;
    case GOLO_TK_FUNC_ATAN:
      *result = atan(results[0]);
      break;
    case GOLO_TK_FUNC_SQRT:
      *result = sqrt(results[0]);
      break;
    case GOLO_TK_FUNC_LOG:
      *result = log(results[0]);
      break;
    case GOLO_TK_FUNC_LOG10:
      *result = log10(results[0]);
      break;
    case GOLO_TK_FUNC_POW:
      *result = pow(results[0], results[1]);
      break;
    case GOLO_TK_FUNC_ATAN2:
      *result = atan2(results[0], results[1]);
      break;
    case GOLO_TK_FUNC_NONE:
      break;
  }
  
  if (results != NULL)
  {
    free(results);
  }
  
  return ret;
}


/**
 * Ensures that value is between 0 and 255.
 *
 * @param value value to check
 * @returns an unsigned char correspond to the specified value
 */
static unsigned char golo_get_component_value(float value)
{
  value = round(value);
  if (value < 0)
  {
    value = 0;
  }
  else if (value > 255)
  {
    value = 255;
  }
  
  return (unsigned char)value;
}


/**
 * Interprets a move, turn, goto, up or down instruction.
 *
 * @param interpreter interpreter instance to use
 * @param id id of the instruction
 * @param instr instruction to interpret
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_draw_instr(golo_interpreter_t* interpreter,
                                     golo_ast_node_id_t id,
                                     golo_draw_instr_t* instr)
{
  int ret = 0;
  float* values = NULL;
  size_t n;
  
  if (instr->arity > 0)
  {
    values = malloc(instr->arity * sizeof(float));
    for (n = 0; ret == 0 && n < instr->arity; n++)
    {
      values[n] = 0;
      ret = golo_interpret_expr(interpreter, instr->exprs[n], &values[n]);
    }
  }
  
  if (ret == 0)
  {
    switch(id)
    {
      case GOLO_AST_NODE_MOVE:
        canvas_move(&interpreter->canvas, values[0]);
        break;
      case GOLO_AST_NODE_TURN:
        canvas_turn(&interpreter->canvas, values[0]);
        break;
      case GOLO_AST_NODE_GOTO:
        canvas_goto(&interpreter->canvas, values[0], values[1]);
        break;
      case GOLO_AST_NODE_COLOR:
        interpreter->canvas.color.r = golo_get_component_value(values[0]);
        interpreter->canvas.color.g = golo_get_component_value(values[1]);
        interpreter->canvas.color.b = golo_get_component_value(values[2]);
        break;
      case GOLO_AST_NODE_UP:
        interpreter->canvas.isdown = 0;
        break;
      case GOLO_AST_NODE_DOWN:
        interpreter->canvas.isdown = 1;
        break;
      default:
        break;
    }
  }
  
  if (values != NULL)
  {
    free(values);
  }
  
  return ret;
}


/**
 * Interprets an expression.
 *
 * @param interpreter interpreter instance to use
 * @param expr expression to interpret
 * @param result result of the interpretation of the expression
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_expr(golo_interpreter_t* interpreter,
                               golo_expr_t* expr,
                               float* result)
{
  int ret = 0;

  switch (expr->token.id)
  {
    case GOLO_TK_BUILTIN:
      ret = golo_interpret_builtin(interpreter, expr, result);
      break;
    case GOLO_TK_IDENTIFIER:
      ret = golo_interpret_identifier(interpreter, expr, result);
      break;
    case GOLO_TK_NUMBER:
      *result = expr->token.value;
      break;
    case GOLO_TK_UPLUS:
    case GOLO_TK_UMINUS:
    case GOLO_TK_NOT:
      ret = golo_interpret_expr_unary(interpreter, expr, result);
      break;
    case GOLO_TK_MUL:
    case GOLO_TK_DIV:
    case GOLO_TK_MOD:
    case GOLO_TK_PLUS:
    case GOLO_TK_MINUS:
    case GOLO_TK_INF:
    case GOLO_TK_INFEQ:
    case GOLO_TK_SUP:
    case GOLO_TK_SUPEQ:
    case GOLO_TK_EQ:
    case GOLO_TK_NEQ:
      ret = golo_interpret_expr_binary(interpreter, expr, result);
      break;
    case GOLO_TK_AND:
      ret = golo_interpret_expr_and(interpreter, expr, result);
      break;
    case GOLO_TK_OR:
      ret = golo_interpret_expr_or(interpreter, expr, result);
      break;
    default:
      break;
  }
  
  return ret;
}


/**
 * Interprets a set instruction.
 *
 * @param interpreter interpreter instance to use
 * @param set set instruction to interpret
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_set(golo_interpreter_t* interpreter,
                              golo_set_instr_t* set)
{
  int ret = 0;
  golo_variable_t* variable = set->variable;
  list_element_t* elem = NULL;
  float value = 0;
  
  if (variable != NULL)
  {
    elem = variable->values.first;
    if (elem != NULL && elem->data != NULL)
    {
      ret = golo_interpret_expr(interpreter, set->expr, &value);
      *((float*)elem->data) = value;
    }
  }
  
  return ret;
}


/**
 * Interprets a if or a while instruction.
 *
 * @param interpreter interpreter instance to use
 * @param alter if or while instruction to interpret
 * @param returned set to 1 if a value is returned, set to 0 otherwise
 * @param retvalue value modified by the one returned if the if or the else bloc
 *                 contains a return instruction)
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_alternative(golo_interpreter_t* interpreter,
                                      golo_alternative_t* alter,
                                      int* returned,
                                      float* retvalue)
{
  int ret = 0;
  float result = 0;
  int ok = 1;
  int do_else = 1;
  golo_ast_node_t* end_node = alter->else_node;
  
  if (end_node == NULL)
  {
    end_node = alter->end_node;
  }
  
  do
  {
    result = 0;
    ret = golo_interpret_expr(interpreter, alter->condition, &result);
    ok = ret == 0 && result != 0.0;
            
    if (ok)
    {
      do_else = 0;
      ret = golo_interpret_bloc(interpreter, alter->alter_node->next, end_node,
        returned, retvalue);
    }
  } while(ret == 0 && ok && alter->is_loop);
  
  if (ret == 0 && do_else && alter->else_node != NULL)
  {
    ret = golo_interpret_bloc(interpreter, alter->else_node->next,
        alter->end_node, returned, retvalue);
  }
  
  return ret;
}


/**
 * Interprets a bloc of instructions.
 *
 * @param interpreter interpreter instance to use
 * @param first first instruction of the bloc to interpret
 * @param last last instruction of the bloc (this instruction is not
 *             interpreted because it is a end instruction which do nothing)
 * @param returned set to 1 if a value is returned, set to 0 otherwise
 * @param retvalue value modified by the one returned if the bloc contains a
 *                 return instruction)
 * @returns 0 in case of success, 1 otherwise
 */
static int golo_interpret_bloc(golo_interpreter_t* interpreter,
                               golo_ast_node_t* first,
                               golo_ast_node_t* last,
                               int* returned,
                               float* retvalue)
{
  golo_ast_node_t* node;
  golo_alternative_t* alter;
  float tmp = 0;
  int ret = 0;
  
  for (node = first;
       node != last && node != NULL && ret == 0 && !(*returned);
       node = node->next)
  {
    switch (node->id)
    {
      case GOLO_AST_NODE_BEGIN:
        ret = golo_interpret_begin(interpreter, node->data);
        break;
      case GOLO_AST_NODE_MOVE:
      case GOLO_AST_NODE_TURN:
      case GOLO_AST_NODE_GOTO:
      case GOLO_AST_NODE_UP:
      case GOLO_AST_NODE_DOWN:
      case GOLO_AST_NODE_COLOR:
        ret = golo_interpret_draw_instr(interpreter, node->id, node->data);
        break;
      case GOLO_AST_NODE_EXPR:
        ret = golo_interpret_expr(interpreter, node->data, &tmp);
        break;
      case GOLO_AST_NODE_RETURN:
        ret = golo_interpret_expr(interpreter, node->data, retvalue);
        *returned = 1;
        break;
      case GOLO_AST_NODE_SET:
        ret = golo_interpret_set(interpreter, node->data);
        break;
      case GOLO_AST_NODE_ALTERNATIVE:
        alter = node->data;
        ret = golo_interpret_alternative(interpreter, alter, returned,
          retvalue);
        node = alter->end_node;
        break;
      default:
        break;
    }
  }
  
  return ret;
}


/**
 * Interprets a GOLO program. This function calls golo_parse to parse the input
 * program.
 *
 * @param interpreter interpreter instance to use
 * @param input input program to interpret
 * @returns 0 in case of success, 1 otherwise
 */
int golo_interpret(golo_interpreter_t* interpreter, FILE* input)
{
  int ret = 0;
  float* pvalue;
  float tmp = 0;
  int returned = 0;
  golo_begin_t* begin;
  golo_variable_t* variable;
  list_element_t* elem;
  
  ret = golo_parse(&interpreter->parser, input);
  if (ret == 0)
  {
    begin = interpreter->parser.begin->data;

    // We initialize all variables.
    for (elem = begin->function->variables.first; elem != NULL;
         elem = elem->next)
    {
      variable = elem->data;
      pvalue = malloc(sizeof(float));
      *pvalue = 0;
      list_push_first(&variable->values, pvalue);
    }
    
    ret = golo_interpret_bloc(interpreter, begin->function->func_node,
      begin->function->end_node, &returned, &tmp);
    if (ret != 0)
    {
      golo_error_print();
    }
    
    // We free all variables.
    for (elem = begin->function->variables.first; elem != NULL;
         elem = elem->next)
    {
      variable = elem->data;
      list_free(&variable->values, 1);
    }
  }
  
  return ret;
}


/**
 * Frees a interpreter instance.
 *
 * @param interpreter interpreter instance to free
 */
void golo_free_interpreter(golo_interpreter_t* interpreter)
{
  golo_free_parser(&interpreter->parser);
  canvas_free(&interpreter->canvas);
}
