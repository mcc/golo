function Vk n
  if n == 0
    move 2
  else
    Vk n - 1
    turn -60
    Vk n - 1
    turn 120
    Vk n - 1
    turn -60
    Vk n - 1
  end
end


begin 600 600
  set i 0
  set n 5
  
  up
  goto 50 150
  down
  
  while i < 3
    Vk n
    turn 120
    set i i + 1
  end
end
