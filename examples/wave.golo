function Wave freq yoffset amp
  set x 0
  
  while x < @w
    goto x ((1 + Sin (x / @w * 3.14159 * freq * 2)) * amp) + yoffset
    set x x + 1
  end
end


begin 300 200
  set y 0
  set maxy 100
  set freq 2
  set amp (@h - maxy) / 2
  set margin 5
  
  while y <= maxy
    if y <= margin || y >= maxy - margin
      color 0 0 0
    else
      set c (y / maxy) * 255
      color c / 4 128 c
    end
    up
    goto 0 @h / 2
    down
    Wave freq y amp
    set y y + 1
  end
end
