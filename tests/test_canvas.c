#include "test_canvas.h"
#include "math.h"
#include "assert.h"
#include "canvas.h"


static void golo_test_canvas_init()
{
  canvas_t canvas;
  
  canvas_init(&canvas, 100, 250);
  
  golo_assert(canvas.width == 100);
  golo_assert(canvas.height == 250);
  golo_assert(canvas.x == 50);
  golo_assert(canvas.y == 125);
  golo_assert(canvas.angle == 0);
  golo_assert(canvas.isdown == 1);
  golo_assert(canvas.color.r == 0);
  golo_assert(canvas.color.g == 0);
  golo_assert(canvas.color.b == 0);
  golo_assert(canvas.pixels != NULL);
  
  canvas_free(&canvas);
}


static void golo_test_canvas_turn()
{
  canvas_t canvas;
  
  canvas_init(&canvas, 1, 1);
  
  canvas_turn(&canvas, 50);
  golo_assert(canvas.angle == 50);
  canvas_turn(&canvas, 365);
  golo_assert(canvas.angle == 55);
  canvas_turn(&canvas, -6);
  golo_assert(canvas.angle == 49);
  
  canvas_free(&canvas);
}


static void golo_test_canvas_move()
{
  canvas_t canvas;
  
  canvas_init(&canvas, 100, 100);
  
  canvas_move(&canvas, 100);
  golo_assert(canvas.x == 150);
  golo_assert(canvas.y == 50);
  
  canvas_turn(&canvas, 45);
  canvas_move(&canvas, 50);
  golo_assert(round(canvas.x) == 185);
  golo_assert(round(canvas.y) == 85);
  
  canvas_move(&canvas, -50);
  canvas_turn(&canvas, -45);
  canvas_move(&canvas, -100);
  golo_assert(round(canvas.x) == 50);
  golo_assert(round(canvas.y) == 50);
  
  canvas_free(&canvas);
}


static void golo_test_canvas_goto()
{
  canvas_t canvas;
  
  canvas_init(&canvas, 100, 100);
  
  canvas_goto(&canvas, 0, 0);
  golo_assert(canvas.x == 0);
  golo_assert(canvas.y == 0);
  
  canvas_goto(&canvas, 250, -28);
  golo_assert(canvas.x == 250);
  golo_assert(canvas.y == -28);
  
  canvas_free(&canvas);
}


void golo_test_canvas()
{
  golo_test_canvas_init();
  golo_test_canvas_turn();
  golo_test_canvas_move();
  golo_test_canvas_goto();
}
