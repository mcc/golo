/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef GOLO_CANVAS_H
#define GOLO_CANVAS_H


/**
 * color_t is an RGB color.
 */
typedef struct color_t
{
  unsigned char r; /**< Red component */
  unsigned char g; /**< Green component */
  unsigned char b; /**< Blue component */
} color_t;


/**
 * A canvas is an array of width * height color_t elements.
 */
typedef struct canvas_t
{
  int       width;  /**< Width of the canvas in pixels */
  int       height; /**< Height of the canvas in pixels */
  double    x;      /**< Current x position */
  double    y;      /**< Current y position */
  double    angle;  /**< Current angle in degrees */
  int       isdown; /**< 0 if the pen is up, 1 if it is down */
  color_t   color;  /**< Current color */
  color_t** pixels; /**< Pixels data */
} canvas_t;


void canvas_init(canvas_t* canvas, int width, int height);
void canvas_draw_line(canvas_t* canvas, int x1, int y1, int x2, int y2);
void canvas_turn(canvas_t* canvas, double angle);
void canvas_move(canvas_t* canvas, double distance);
void canvas_goto(canvas_t* canvas, double x, double y);
void canvas_export(canvas_t* canvas, FILE* output);
void canvas_free(canvas_t* canvas);


#endif
