/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file parser.c
 * 
 * This file provides functions to parse instructions given as a sequence of
 * tokens.
 */


#include <stdlib.h>
#include <string.h>
#include "parser.h"
#include "error.h"


static int golo_parse_subexpr(golo_parser_t* parser, golo_expr_t* expr,
                              int* level, int advance);


/**
 * Returns the current function (or begin bloc).
 *
 * @param parser current parser instance
 * @returns the current function
 */
golo_function_t* golo_get_current_function(golo_parser_t* parser)
{
  golo_ast_node_t* node = NULL;
  golo_function_t* func = NULL;
  golo_begin_t* begin = NULL;
  list_element_t* elem = NULL;  
  
  for (elem = parser->blocs.first; func == NULL && elem != NULL;
       elem = elem->next)
  {
    node = elem->data;
    if (node != NULL)
    {
      if (node->id == GOLO_AST_NODE_BEGIN)
      {
        begin = node->data;
        func = begin->function;
      }
      else if (node->id == GOLO_AST_NODE_FUNCTION)
      {
        func = node->data;
      }
    }
  }

  return func;
}


/**
 * Searches and returns the function named with the given string.
 *
 * @param parser current parser instance
 * @param token token corresponding to the function to search
 * @returns the function node if it exists, NULL otherwise
 */
static golo_function_t* golo_get_function(golo_parser_t* parser, golo_token_t* token)
{
  size_t read_size = 0;
  golo_function_t* func = map_get(&parser->functions, token->name, token->size,
                                  &read_size);
  if (read_size != token->size)
  {
    func = NULL;
  }

  return func;
}


/**
 * Return the variable associated to the name of the token.
 *
 * @param parser current parser instance
 * @param token token corresponding to the variable to search
 * @returns A pointer to the variable if it is defined, NULL otherwise
 */
static golo_variable_t* golo_get_variable(golo_parser_t* parser,
                                          golo_token_t* token)
{
  size_t read_size = 0;
  golo_variable_t* variable = NULL;
  golo_function_t* func = NULL;

  // We check if the identifier corresponds to a local variable.
  func = golo_get_current_function(parser);
  if (func != NULL)
  {
    variable = map_get(&func->variables_map, token->name, token->size,
                       &read_size);
    if (read_size != token->size)
    {
      variable = NULL;
    }
  }
  
  return variable;
}


/**
 * Declares a variable in a function.
 * 
 * @param function function where the variable have to be declared
 * @param token token corresponding to the variable to declare
 * @returns the declared variable
 */
static golo_variable_t* golo_declare_variable(golo_function_t* function,
                                              golo_token_t* token)
{
  golo_variable_t* variable = malloc(sizeof(golo_variable_t));
  
  list_init(&variable->values);
  map_set(&function->variables_map, token->name, token->size, variable);
  list_push_last(&function->variables, variable);
  
  return variable;
}


/**
 * Declares a function if it doesn't exists.
 *
 * @param parser current parser instance
 * @param token token corresponding to the function to declare
 * @returns the declared function or the existing one of any
 */
static golo_function_t* golo_declare_function(golo_parser_t* parser,
                                              golo_token_t* token)
{
  golo_function_t* function = NULL;
  
  if (token != NULL)
  {
    function = golo_get_function(parser, token);
  }

  if (function == NULL)
  {
    function = malloc(sizeof(golo_function_t));
    function->func_node = NULL;
    function->end_node = NULL;
    map_init(&function->variables_map);
    list_init(&function->variables);
    
    if (token != NULL)
    {
      function->arity = token->arity;
      map_set(&parser->functions, token->name, token->size, function);
    }
    else
    {
      function->arity = 0;
    }
  }
  
  return function;
}


/**
 * Parses an identifier or a function call node. If the identifier is not a
 * function (built-in or user defined), we try to parse the following tokens as
 * as function parameters.
 *
 * @param parser current parser instance
 * @param identifier identifier node
 * @param level current level of the expression which is beeing parsed
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_identifier(golo_parser_t* parser,
                                 golo_expr_t* identifier, int* level)
{
  int ret = 0;
  golo_expr_t* tmp = NULL;
  int lvl = *level;
  int expected_arity = -1;
    
  // We search for the corresponding function or variable.
  identifier->variable = NULL;
  identifier->function = NULL;
  if (identifier->token.id != GOLO_TK_BUILTIN)
  {
    identifier->function = golo_get_function(parser, &parser->lexer.token);
    if (identifier->function == NULL)
    {
      identifier->variable = golo_get_variable(parser, &parser->lexer.token);
    }
  }
  
  // If the identifier corresponds to a built-in or user defined function call,
  // we have to parse parameters.
  if (identifier->token.id == GOLO_TK_BUILTIN || identifier->variable == NULL)
  {
    // If the token is a buil-in function or a user defined function, we have
    // to parse the expected number of expressions. If the token is an undefined
    // function name, we parse the maximum number of expressions because the
    // function is maybe defined after the call.
    if (identifier->token.id == GOLO_TK_BUILTIN)
    {
      expected_arity = identifier->token.arity;
      identifier->token.arity = 0;
    }
    else if (identifier->function != NULL)
    {
      
      expected_arity = identifier->function->arity;
    }
    else if (identifier->function == NULL)
    {
      // If identifier->function is NULL, it's an implicit declaration so we
      // declare the function and we parse the maximum of parameters.
      identifier->function = golo_declare_function(parser, &identifier->token);
    }
    
    golo_get_next_token(&parser->lexer);
        
    // We stop parsing child expressions if :
    //  - an error occurred when parsing an expression.
    //  - the read token cannot appear at the beginning of an expression.
    //  - the current expression level has changed.
    //  - a sufficient number of expressions has been parsed.
    while (ret == 0 &&
           golo_token_is_value(parser->lexer.token.id) &&
           lvl == *level &&
           ((int)identifier->token.arity < expected_arity ||
            expected_arity < 0))
    {      
      tmp = malloc(sizeof(golo_expr_t));
      tmp->child = NULL;
      tmp->parent = NULL;
      tmp->variable = NULL;
      tmp->function = NULL;
      
      golo_init_token(&parser->lexer, &tmp->token);
      ret = golo_parse_subexpr(parser, tmp, level, 0);
      
      if (ret == 0)
      {
        identifier->child = realloc(identifier->child,
          (identifier->token.arity + 1) * sizeof(golo_expr_t*));
        while (tmp->parent != NULL)
        {
          tmp = tmp->parent;
        }
        
        tmp->parent = identifier;
        identifier->child[identifier->token.arity] = tmp;
        identifier->token.arity++;
      }
    }
  }
  else
  {
    golo_get_next_token(&parser->lexer);
  }
  
  return ret;
}


/**
 * Prints an expression to the output.
 *
 * @param expr expression node to print
 * @param level current expression level
 */
/*static*/ void golo_expr_print(golo_expr_t* expr, int level)
{
  size_t n;
  
  if (expr != NULL)
  {
    printf("%*s%d %d %s\n", level, "", expr->token.id, (int)expr->token.value,
           expr->token.name);
    for (n = 0; n < expr->token.arity; n++)
    {
      golo_expr_print(expr->child[n], level + 2);
    }
  }
  else
  {
    printf("%*s(null)\n", level, "");
  }
}


/**
 * Inserts a node in an exression respecting the operators priority.
 *
 * @param expr expression node to modify
 * @param ins node to insert
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_expr_insert(golo_expr_t* expr, golo_expr_t* ins)
{
  int priority = ins->token.priority;
  int cur_priority = expr->token.priority;
  int ind = expr->token.arity - 1;
  
  if (priority >= cur_priority)
  {
    if (expr->parent == NULL)
    {
      // Adding to the root.
      expr->parent = ins;
      ins->child[0] = expr;
      ins->parent = NULL;
    }
    else
    {
      // Adding above.
      golo_expr_insert(expr->parent, ins);
    }
  }
  else if (ind >= 0)
  {
    // Adding bellow.
    if (ins->child != NULL && ins->child[0] == NULL)
    {
      ins->child[0] = expr->child[ind];
      if (ins->child[0] != NULL)
      {
        ins->child[0]->parent = ins;
      }
    }
    expr->child[ind] = ins;
    ins->parent = expr;
  }
  
  return 0;
}


/**
 * Validates an expression by checking that any child node is defined ans valid.
 *
 * @param lexer current lexer instance
 * @param expr expression to validate
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_validate_expr(golo_lexer_t* lexer, golo_expr_t* expr)
{
  size_t n;
  int ret = 0;
  
  if (expr->token.id == GOLO_TK_EOL)
  {
    ret = 1;
    golo_error_set(GOLO_ERROR_EXPECTED_EXPR, lexer->token.line,
      lexer->token.column);
  }
  else if (expr->token.arity > 0 && expr->child == NULL)
  {
    ret = 1;
    golo_error_set(GOLO_ERROR_MISSING_PARAM, lexer->token.line,
      lexer->token.column);
  }
  
  
  for (n = 0; ret == 0 && n < expr->token.arity; n++)
  {
    if (expr->child[n] == NULL)
    {
      ret = 1;
      golo_error_set(GOLO_ERROR_SYNTAX, expr->token.line,
        expr->token.column);
    }
    else
    {
      ret = golo_validate_expr(lexer, expr->child[n]);
    }
  }
  
  return ret;
}


/**
 * Parses a boolean or arithmetic subexpression.
 *
 * @param parser current parser instance
 * @param expr subexpression to parse
 * @param level current expression level
 * @param advance if equals 1 we read a token before to parse the expression
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_subexpr(golo_parser_t* parser, golo_expr_t* expr,
                              int* level, int advance)
{
  golo_expr_t* tmp = NULL;
  int insert = 0;
  int was_value = 0;
  int ret = 0;
  size_t n;
  golo_lexer_t* lexer = &parser->lexer;
  
  if (advance)
  {
    golo_get_next_token(lexer);
  }
  
  // We stop if :
  //  - a token that does not appear in an expression is read.
  //  - the end of the line is read
  //  - a value (number, subexpression, identifier, built-in function)
  //    was read after an other value, for example 1 + 2 3.
  while (ret == 0 && (lexer->token.priority >= 0 ||
                      lexer->token.id == GOLO_TK_LPAR) &&
         (!was_value || !golo_token_is_value(lexer->token.id)))
  {
    was_value = golo_token_is_value(lexer->token.id);
    
    if (tmp == NULL)
    {
      tmp = expr;
    }
    else
    {
      tmp = malloc(sizeof(golo_expr_t));
      insert = 1;
    }
    tmp->parent = NULL;
    tmp->child = NULL;
    tmp->variable = NULL;
    tmp->function = NULL;
    
    if (lexer->token.arity > 0)
    {
      tmp->child = malloc(lexer->token.arity * sizeof(golo_expr_t*));
      for (n = 0; n < lexer->token.arity; n++)
      {
        tmp->child[n] = NULL;
      }
    }
    
    if (lexer->token.id == GOLO_TK_LPAR)
    {
      (*level)++;
      ret = golo_parse_subexpr(parser, tmp, level, 1);
      while (tmp->parent != NULL)
      {
        tmp = tmp->parent;
      }
      if (lexer->token.id == GOLO_TK_RPAR)
      {
        (*level)--;
        golo_get_next_token(lexer);
      }
      else
      {
        golo_error_set(GOLO_ERROR_MISSING_RPAR,
          parser->lexer.token.line, parser->lexer.token.column);
        ret = 1;
      }
      tmp->token.priority = 0;
    }
    else if (lexer->token.id == GOLO_TK_IDENTIFIER ||
             lexer->token.id == GOLO_TK_BUILTIN)
    {
      tmp->token = lexer->token;
      ret = golo_parse_identifier(parser, tmp, level);
    }
    else
    {
      tmp->token = lexer->token;
      golo_get_next_token(lexer);
    }
    
    if (insert)
    {
      golo_expr_insert(expr, tmp);
    }
    expr = tmp;
  }
  
  return ret;
}


/**
 * Parses a boolean or arithmetic expression.
 *
 * @param parser current parser instance
 * @param expr expression to parse
 * @param advance if equals 1 we read a token before to parse the expression
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_expr(golo_parser_t* parser, golo_expr_t** expr,
                           int advance)
{
  int ret = 0;
  int level = 0;

  *expr = malloc(sizeof(golo_expr_t));
  (*expr)->parent = NULL;
  (*expr)->child = NULL;
  (*expr)->variable = NULL;
  (*expr)->function = NULL;
  golo_init_token(&parser->lexer, &(*expr)->token);
  
  ret = golo_parse_subexpr(parser, *expr, &level, advance);
  while ((*expr)->parent != NULL)
  {
    *expr = (*expr)->parent;
  }
    
  if (ret == 0)
  {
    if (level < 0)
    {
      golo_error_set(GOLO_ERROR_MISSING_LPAR, (*expr)->token.line, 0);
      ret = 1;
    }
    else if (level > 0)
    {
      golo_error_set(GOLO_ERROR_MISSING_RPAR, (*expr)->token.line, 0);
      ret = 1;
    }
    else
    {
      ret = golo_validate_expr(&parser->lexer, *expr);
    }
  }
  
  return ret;
}


/**
 * Parses a positive integer value.
 *
 * @param parser current parser instance
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_positive_integer(golo_parser_t* parser)
{
  int ret = 0;
  
  golo_get_next_token(&parser->lexer);
  if (parser->lexer.token.id != GOLO_TK_NUMBER ||
      parser->lexer.token.value <= 0 ||
      parser->lexer.token.value != (int)parser->lexer.token.value)
  {
    golo_error_set(GOLO_ERROR_EXPECTED_POSITIVE_INTEGER,
      parser->lexer.token.line, parser->lexer.token.column);
    ret = 1;
  }
  
  return ret;
}


/**
 * Parses a begin instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_begin(golo_parser_t* parser, golo_ast_node_t* node)
{
  int ret = 0;
  golo_begin_t* begin = NULL;
  
  if (parser->begin != NULL)
  {
    golo_error_set(GOLO_ERROR_BEGIN_REDECLARATION,
      parser->lexer.token.line, parser->lexer.token.column);
    ret = 1;
  }
  else
  {
    begin = malloc(sizeof(golo_begin_t));
    begin->function = golo_declare_function(parser, NULL);
    begin->function->func_node = node;
  
    parser->begin = node;
    list_push_first(&parser->blocs, node);
    
    ret = golo_parse_positive_integer(parser);
    if (ret == 0)
    {
      begin->width = (int)parser->lexer.token.value;
      ret = golo_parse_positive_integer(parser);
      if (ret == 0)
      {
        begin->height = (int)parser->lexer.token.value;
      }
      golo_get_next_token(&parser->lexer);
    }
  }
  
  node->id = GOLO_AST_NODE_BEGIN;
  node->data = begin;
  
  return ret;
}


/**
 * Parses a function instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_function(golo_parser_t* parser, golo_ast_node_t* node)
{
  int ret = 0;
  golo_function_t* func = NULL;
      
  list_push_first(&parser->blocs, node);
  
  golo_get_next_token(&parser->lexer);
  if (parser->lexer.token.id != GOLO_TK_IDENTIFIER)
  {
    golo_error_set(GOLO_ERROR_EXPECTED_IDENTIFIER, parser->lexer.token.line,
      parser->lexer.token.column);
    ret = 1;
  }
  else
  {
    // We declare the function.
    func = golo_declare_function(parser, &parser->lexer.token);
    func->func_node = node;
    func->arity = 0;
    
    // We parse the formal parameters and associate them to the function.
    golo_get_next_token(&parser->lexer);
    while (parser->lexer.token.id == GOLO_TK_IDENTIFIER)
    {
      func->arity++;
      golo_declare_variable(func, &parser->lexer.token);
      golo_get_next_token(&parser->lexer);
    }
  }
  
  node->id = GOLO_AST_NODE_FUNCTION;
  node->data = func;
    
  return ret;
}


/**
 * Parses a return instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_return(golo_parser_t* parser, golo_ast_node_t* node)
{
  int ret = 0;
  golo_expr_t* expr = NULL;
  
  ret = golo_parse_expr(parser, &expr, 1);
    
  node->id = GOLO_AST_NODE_RETURN;
  node->data = expr;
  
  return ret;
}


/**
 * Parses a set instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_set(golo_parser_t* parser, golo_ast_node_t* node)
{
  int ret = 0;
  golo_set_instr_t* set = malloc(sizeof(golo_set_instr_t));
    
  set->variable = NULL;
  set->expr = NULL;
  
  golo_get_next_token(&parser->lexer);
  if (parser->lexer.token.id != GOLO_TK_IDENTIFIER)
  {
    golo_error_set(GOLO_ERROR_EXPECTED_IDENTIFIER, parser->lexer.token.line,
      parser->lexer.token.column);
    ret = 1;
  }
  else
  {
    if (golo_get_function(parser, &parser->lexer.token) != NULL)
    {
      golo_error_set(GOLO_ERROR_SET_FUNCTION, parser->lexer.token.line,
        parser->lexer.token.column);
      ret = 1;  
    }
    else
    {
      // If the variable doesn't exist, we declare it.
      set->variable = golo_get_variable(parser, &parser->lexer.token);
      if (set->variable == NULL)
      {
        set->variable = golo_declare_variable(golo_get_current_function(parser),
                                              &parser->lexer.token);
      }
      
      // We try to parse the expression of the set instruction.
      ret = golo_parse_expr(parser, &set->expr, 1);
    }
  }
  
  node->id = GOLO_AST_NODE_SET;
  node->data = set;
  
  return ret;
}


/**
 * Parses a if or while instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @param is_loop indicates if the alternative is a WHILE or a IF
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_alternative(golo_parser_t* parser, golo_ast_node_t* node,
                                  int is_loop)
{
  int ret = 0;
  golo_alternative_t* alter = malloc(sizeof(golo_alternative_t));
  
  alter->is_loop = is_loop;
  alter->condition = NULL;
  alter->alter_node = node;
  alter->else_node = NULL;
  alter->end_node = NULL;
  
  list_push_first(&parser->blocs, node);
  
  node->id = GOLO_AST_NODE_ALTERNATIVE;
  node->data = alter;
  
  ret = golo_parse_expr(parser, &alter->condition, 1);
  
  return ret;
}


/**
 * Parses an else or end instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @param is_end indicates if the node is a END or a ELSE
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_else_end(golo_parser_t* parser, golo_ast_node_t* node,
                               int is_end)
{
  int ret = 0;
  
  list_element_t* bloc = parser->blocs.first;
  golo_ast_node_t* bloc_node = NULL;
  golo_ast_node_t** pnode = NULL;
  
  if (bloc == NULL)
  {
    golo_error_set(
      is_end ? GOLO_ERROR_UNEXPECTED_END : GOLO_ERROR_UNEXPECTED_ELSE,
      parser->lexer.token.line, parser->lexer.token.column);
    ret = 1;
  }
  else
  {
    bloc_node = bloc->data;
    if (bloc_node->id != GOLO_AST_NODE_ALTERNATIVE && !is_end)
    {
      golo_error_set(GOLO_ERROR_UNEXPECTED_ELSE, parser->lexer.token.line,
        parser->lexer.token.column);
      ret = 1;
    }
    else
    {
      if (is_end)
      {
        list_pop_first(&parser->blocs);
      }
      
      if (bloc_node->id == GOLO_AST_NODE_FUNCTION)
      {
        pnode = &((golo_function_t*)bloc_node->data)->end_node;
      }
      else if (bloc_node->id == GOLO_AST_NODE_BEGIN)
      {        
        pnode = &((golo_begin_t*)bloc_node->data)->function->end_node;
      }
      else if (bloc_node->id == GOLO_AST_NODE_ALTERNATIVE)
      {
        if (is_end)
        {
          pnode = &((golo_alternative_t*)bloc_node->data)->end_node;
        }
        else
        {
          pnode = &((golo_alternative_t*)bloc_node->data)->else_node;
        }
      }
      
      if (pnode != NULL)
      {
        if (*pnode != NULL)
        {
          golo_error_set(
            is_end ? GOLO_ERROR_UNEXPECTED_END : GOLO_ERROR_UNEXPECTED_ELSE,
            parser->lexer.token.line, parser->lexer.token.column);
          ret = 1;
        }
        *pnode = node;
      }
      
      golo_get_next_token(&parser->lexer);
    }
  }
  
  node->id = is_end ? GOLO_AST_NODE_END : GOLO_AST_NODE_ELSE;
  node->data = NULL;
  
  return ret;
}


/**
 * Parses a move, turn, goto, color, up or down instruction.
 *
 * @param parser current parser instance
 * @param node node to modify
 * @param token token corresponding to the instruction to parse
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_parse_draw_instr(golo_parser_t* parser, golo_ast_node_t* node,
                                 golo_token_t* token)
{
  int ret = 0;
  int n;
  int arity = token->arity;
  golo_token_id_t tokenid = token->id;
  golo_draw_instr_t* instr = malloc(sizeof(golo_draw_instr_t));
  
  instr->exprs = NULL;
  instr->arity = token->arity;
  if (token->arity > 0)
  {
    instr->exprs = malloc(arity * sizeof(golo_draw_instr_t*));
    for (n = 0; n < arity; n++)
    {
      instr->exprs[n] = NULL;
      if (ret == 0)
      {
        ret = golo_parse_expr(parser, &instr->exprs[n], n == 0);
      }
    }
  }
  else
  {
    golo_get_next_token(&parser->lexer);
  }
  
  switch (tokenid)
  {
    default:
    case GOLO_TK_MOVE:
      node->id = GOLO_AST_NODE_MOVE;
      break;
    case GOLO_TK_TURN:
      node->id = GOLO_AST_NODE_TURN;
      break;
    case GOLO_TK_GOTO:
      node->id = GOLO_AST_NODE_GOTO;
      break;
    case GOLO_TK_COLOR:
      node->id = GOLO_AST_NODE_COLOR;
      break;
    case GOLO_TK_UP:
      node->id = GOLO_AST_NODE_UP;
      break;
    case GOLO_TK_DOWN:
      node->id = GOLO_AST_NODE_DOWN;
      break;
  }
  
  node->data = instr;
  
  return ret;
}


/**
 * Validates the context of an instruction. For example an else instruction
 * cannot appear outside a while or if bloc.
 *
 * @param parser current parser instance
 * @param id id of the instruction token to validate
 * @returns 0 in case of success, 1 otherwise.
 */
static int golo_validate_context(golo_parser_t* parser, golo_token_id_t id)
{
  int ret = 0;
  int inside = golo_is_token_inside_bloc(id) &&
               !golo_is_token_outside_bloc(id);
  int outside = golo_is_token_outside_bloc(id) &&
                !golo_is_token_inside_bloc(id);
  
  if (inside && parser->blocs.first == NULL)
  {
    golo_error_set(GOLO_ERROR_INSTR_OUTSIDE_BLOC, parser->lexer.token.line,
      parser->lexer.token.column);
    ret = 1;
  }
  else if (outside && parser->blocs.first != NULL)
  {
    golo_error_set(GOLO_ERROR_FUNC_INSIDE_BLOC, parser->lexer.token.line,
      parser->lexer.token.column);
    ret = 1;
  }
  
  return ret;
}


/**
 * Initializes the parser.
 *
 * @param parser parser to initialize
 */
void golo_init_parser(golo_parser_t* parser)
{
  golo_init_lexer(&parser->lexer);
  map_init(&parser->functions);
  list_init(&parser->nodes);
  list_init(&parser->blocs);
  parser->begin = NULL;
}


/**
 * Parses an instruction given as a string.
 *
 * @param parser current parser instance
 * @param instr instruction to parse
 * @returns 0 in case of success, 1 otherwise.
 */
int golo_parse_instr(golo_parser_t* parser, char* instr)
{
  int ret = 0;
  golo_ast_node_t* node = NULL;
    
  golo_set_lexer_instr(&parser->lexer, instr);
  golo_get_next_token(&parser->lexer);
  
  if (parser->lexer.token.id != GOLO_TK_EOL)
  {
    ret = golo_validate_context(parser, parser->lexer.token.id);
    if (ret == 0)
    { 
      node = malloc(sizeof(golo_ast_node_t));
      node->data = NULL;
      switch(parser->lexer.token.id)
      {
        case GOLO_TK_BEGIN:
          ret = golo_parse_begin(parser, node);
          break;
          
        case GOLO_TK_FUNCTION:
          ret = golo_parse_function(parser, node);      
          break;
          
        case GOLO_TK_RETURN:
          ret = golo_parse_return(parser, node);
          break;
          
        case GOLO_TK_SET:
          ret = golo_parse_set(parser, node);
          break;
          
        case GOLO_TK_IF:
        case GOLO_TK_WHILE:
          ret = golo_parse_alternative(parser, node,
            parser->lexer.token.id == GOLO_TK_WHILE);
          break;
        
        case GOLO_TK_ELSE:
        case GOLO_TK_END:
          ret = golo_parse_else_end(parser, node,
            parser->lexer.token.id == GOLO_TK_END);
          break;
          
        case GOLO_TK_MOVE:
        case GOLO_TK_TURN:
        case GOLO_TK_GOTO:
        case GOLO_TK_COLOR:
        case GOLO_TK_UP:
        case GOLO_TK_DOWN:
          ret = golo_parse_draw_instr(parser, node, &parser->lexer.token);
          break;
        
        default:
          node->id = GOLO_AST_NODE_EXPR;
          ret = golo_parse_expr(parser, (golo_expr_t**)&node->data, 0);
          break;
      }
    }
  }
  
  if (ret == 0 && parser->lexer.token.id != GOLO_TK_EOL)
  {
    golo_error_set(GOLO_ERROR_EXPECTED_EOL, parser->lexer.token.line,
      parser->lexer.token.column);
    ret = 1;
  }
  
  if (node != NULL)
  {
    node->next = NULL;
    if (parser->nodes.last != NULL)
    {
      ((golo_ast_node_t*)parser->nodes.last->data)->next = node;
    }
    list_push_last(&parser->nodes, node);
  }
  
  return ret;
}


/**
 * Parses an input program.
 *
 * @param parser current parser instance
 * @param input input to parse
 * @returns 0 in case of success, 1 otherwise.
 */
int golo_parse(golo_parser_t* parser, FILE* input)
{
  const int Bufsize = 1024;
  char buffer[Bufsize];
  int ret = 0;
    
  while (fgets(buffer, Bufsize, input) != NULL)
  {
    ret = golo_parse_instr(parser, buffer);
    if (ret != 0)
    {
      golo_error_print();
    }
  }
  
  // We check that the program is well constructed.
  if (parser->begin == NULL)
  {
    golo_error_set(GOLO_ERROR_MISSING_BEGIN, 0, 0);
    ret = 1;
  }
  else if (((golo_begin_t*)parser->begin->data)->function->end_node == NULL)
  {
    golo_error_set(GOLO_ERROR_MISSING_BEGIN_END, 0, 0);
    ret = 1;
  }

  if (ret != 0)
  {
    golo_error_print();
  }
  
  return ret;
}


/**
 * Frees the memory occupied by an expression.
 *
 * @param expr expression to free
 */
static void golo_expr_free(golo_expr_t* expr)
{
  size_t n;
  
  if (expr != NULL)
  {
    for (n = 0; n < expr->token.arity; n++)
    {
      if (expr->child[n] != NULL)
      {
        golo_expr_free(expr->child[n]);
      }
      expr->child[n] = NULL;
    }
    
    free(expr->child);
    expr->child = NULL;
    free(expr);
  }
}


/**
 * Frees the memory occupied by a draw instruction.
 *
 * @param node node to free
 */
static void golo_draw_instr_free(golo_ast_node_t* node)
{
  size_t n;
  golo_draw_instr_t* instr = node->data;
  
  if (instr->arity > 0 && instr->exprs != NULL)
  {
    for (n = 0; n < instr->arity; n++)
    {
      if (instr->exprs[n] != NULL)
      {
        golo_expr_free(instr->exprs[n]);
      }
    }
    free(instr->exprs);
    instr->exprs = NULL;
  }
}


/**
 * Frees the memory occupied by a function.
 *
 * @param function function to free
 */
static void golo_function_free(golo_function_t* function)
{
  golo_variable_t* variable;
  list_element_t* elem;

  if (function != NULL)
  {
    for (elem = function->variables.first; elem != NULL; elem = elem->next)
    {
      variable = elem->data;
      if (variable != NULL)
      {
        list_free(&variable->values, 1);
      }
    }
    list_free(&function->variables, 1);
    map_free(&function->variables_map, 0);
  }
}


/**
 * Frees the memory occupied by an AST node.
 *
 * @param node node to free
 */
static void golo_node_free(golo_ast_node_t* node)
{
  golo_set_instr_t* set = NULL;
  golo_alternative_t* alter = NULL;
  golo_begin_t* begin = NULL;
  
  switch (node->id)
  {
    case GOLO_AST_NODE_BEGIN:
      begin = node->data;
      golo_function_free(begin->function);
      free(begin->function);
      break;
    case GOLO_AST_NODE_FUNCTION:
      golo_function_free(node->data);
      node->data = NULL; // Functions are free'd in golo_free_parser.
      break;
    case GOLO_AST_NODE_SET:
      set = node->data;
      golo_expr_free(set->expr);
      break;
    case GOLO_AST_NODE_EXPR:
    case GOLO_AST_NODE_RETURN:
      golo_expr_free(node->data);
      node->data = NULL;
      break;
    case GOLO_AST_NODE_ALTERNATIVE:
      alter = node->data;
      golo_expr_free(alter->condition);
      break;
    case GOLO_AST_NODE_UP:
    case GOLO_AST_NODE_DOWN:
    case GOLO_AST_NODE_MOVE:
    case GOLO_AST_NODE_TURN:
    case GOLO_AST_NODE_GOTO:
    case GOLO_AST_NODE_COLOR:
      golo_draw_instr_free(node);
      break;
    default:
      break;
  }
  
  if (node->data != NULL)
  {
    free(node->data);
    node->data = NULL;
  }
  free(node);
}


/**
 * Frees a parser instance.
 *
 * @param parser parser instance to free
 */
void golo_free_parser(golo_parser_t* parser)
{
  list_element_t* element;
  
  for (element = parser->nodes.first; element != NULL; element = element->next)
  {
    if (element->data != NULL)
    {
      golo_node_free(element->data);
    }
  }
  golo_free_lexer(&parser->lexer);
  map_free(&parser->functions, 1);
  list_free(&parser->nodes, 0);
  list_free(&parser->blocs, 0);
}
