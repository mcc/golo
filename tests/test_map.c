#include "test_map.h"
#include "assert.h"
#include "map.h"


static void golo_test_map_init()
{
  map_t map;
  
  map_init(&map);
  golo_assert(map.next == NULL);
  golo_assert(map.child == NULL);
  golo_assert(map.data == NULL);
}


static void golo_test_map_get_empty()
{
  map_t map;
  void* data;
  size_t read_size = 0;
  
  map_init(&map);
  
  data = map_get(&map, "", 0, &read_size);
  golo_assert(data == NULL);
  golo_assert(read_size == 0);
  
  data = map_get(&map, "", 2, &read_size);
  golo_assert(data == NULL);
  golo_assert(read_size == 0);
  
  data = map_get(&map, "abc", 3, &read_size);
  golo_assert(data == NULL);
  golo_assert(read_size == 0);
  
  map_free(&map, 0);
}



static void golo_test_map_set1()
{
  map_t map;
  void* data;
  size_t read_size;
  
  map_init(&map);
  
  data = map_set(&map, "abc", 3, (int*)1);
  golo_assert(data == NULL);
  
  read_size = 0;
  data = map_get(&map, "abcd", 2, &read_size);
  golo_assert(data == NULL);
  golo_assert(read_size == 2);
  
  read_size = 0;
  data = map_get(&map, "abcd", 3, &read_size);
  golo_assert(data != NULL);
  golo_assert(read_size == 3);
  
  read_size = 0;
  data = map_get(&map, "abc", -1, &read_size);
  golo_assert(data != NULL);
  golo_assert(read_size == 3);
  
  map_free(&map, 0);
}


static void golo_test_map_set2()
{
  map_t map;
  void* data;
    
  map_init(&map);
  
  data = map_set(&map, "abc", -1, (int*)1);
  golo_assert(data == NULL);
  
  data = map_set(&map, "abcd", 3, (int*)2);
  golo_assert(data == (int*)1);
  
  map_free(&map, 0);
}


void golo_test_map()
{
  golo_test_map_init();
  golo_test_map_get_empty();
  golo_test_map_set1();
  golo_test_map_set2();
}
