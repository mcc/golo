begin 1 1
  set a 25
  set b 45
  set c 0
  
  if (Sin a - b) != (Sin a) * (Cos b) - (Cos a) * (Sin b)
    set c c + 1
  end
  
  if (Sin a + b) != (Sin a) * (Cos b) + (Cos a) * (Sin b)
    set c c + 1
  end
  
  if (Cos a - b) != (Cos a) * (Cos b) + (Sin a) * (Sin b)
    set c c + 1
  end
  
  if (Cos a + b) != (Cos a) * (Cos b) - (Sin a) * (Sin b)
    set c c + 1
  end
  
  if (Tan a - b) != ((Tan a) - (Tan b)) / (1 + (Tan a) * (Tan b))
    set c c + 1
  end
  
  if (Tan a + b) != ((Tan a) + (Tan b)) / (1 - (Tan a) * (Tan b))
    set c c + 1
  end
  
  goto c c
end
