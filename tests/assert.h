#ifndef GOLO_ASSERT_H
#define GOLO_ASSERT_H

#include <stdio.h>

#define golo_assert(test) do { \
  if (!(test)) printf("%s : " #test " failed\n", __func__); \
} while(0);

#endif
