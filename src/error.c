/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file error.c
 * 
 * This file provides functions to handle errors in the parsing and
 * interpreting phases.
 */
 
 
#include <stdio.h>
#include "error.h"


static golo_error_t last_error;


/**
 * Sets the current error.
 *
 * @param id identifier of the error to set
 * @param line line where the occor occurred
 * @param column column where the occor occurred
 */
void golo_error_set(golo_error_id_t id, size_t line, size_t column)
{
  last_error.id = id;
  last_error.line = line;
  last_error.column = column;
}


/**
 * Returns the current error.
 *
 * @returns the current error
 */
golo_error_t golo_error_get()
{
  return last_error;
}


/**
 * Prints the current error to the output.
 */
void golo_error_print()
{
  fprintf(stderr, "%d:%d: error: ", last_error.line, last_error.column);
  switch(last_error.id)
  {
    case GOLO_ERROR_SYNTAX:
      fprintf(stderr, "syntax error\n");
      break;
    case GOLO_ERROR_MISSING_RPAR:
      fprintf(stderr, "missing right parenthesis\n");
      break;
    case GOLO_ERROR_MISSING_LPAR:
      fprintf(stderr, "missing left parenthesis\n");
      break;
    case GOLO_ERROR_MISSING_PARAM:
      fprintf(stderr, "missing parameter\n");
      break;
    case GOLO_ERROR_MISSING_BEGIN:
      fprintf(stderr, "missing begin bloc\n");
      break;
    case GOLO_ERROR_MISSING_BEGIN_END:
      fprintf(stderr, "missing end for begin bloc\n");
      break;
    case GOLO_ERROR_UNEXPECTED_END:
      fprintf(stderr, "unexpected end\n");
      break;
    case GOLO_ERROR_UNEXPECTED_ELSE:
      fprintf(stderr, "unexpected else\n");
      break;
    case GOLO_ERROR_EXPECTED_POSITIVE_INTEGER:
      fprintf(stderr, "positive integer expected\n");
      break;
    case GOLO_ERROR_EXPECTED_IDENTIFIER:
      fprintf(stderr, "identifier expected\n");
      break;
    case GOLO_ERROR_EXPECTED_EOL:
      fprintf(stderr, "end of line expected\n");
      break;
    case GOLO_ERROR_EXPECTED_EXPR:
      fprintf(stderr, "expression expected\n");
      break;
    case GOLO_ERROR_BEGIN_REDECLARATION:
      fprintf(stderr, "redeclaration of the begin bloc\n");
      break;
    case GOLO_ERROR_INSTR_OUTSIDE_BLOC:
      fprintf(stderr, "instruction defined outside a bloc\n");
      break;
    case GOLO_ERROR_FUNC_INSIDE_BLOC:
      fprintf(stderr, "function defined inside a bloc\n");
      break;
    case GOLO_ERROR_SET_FUNCTION:
      fprintf(stderr, "set instruction cannot be used with a function name\n");
      break;
    case GOLO_ERROR_UNKNOWN_IDENTIFIER:
      fprintf(stderr, "unknown identifier\n");
      break;
    case GOLO_ERROR_UNKNOWN_FUNCTION:
      fprintf(stderr, "unknown function\n");
      break;
    case GOLO_ERROR_DIVISION_BY_ZERO:
      fprintf(stderr, "division by 0\n");
      break;
    default:
      fprintf(stderr, "unknown error\n");
      break;
  }
}
