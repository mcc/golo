function Line x1 y1 x2 y2
  set oldx @x
  set oldy @y
  
  up 
  goto x1 y1
  down
  goto x2 y2
  up
  goto oldx oldy
  down
end

begin 500 300
  set n 0
  
  while n < @h
    color (n / @h) * 255 0 0
    Line 0 n @w n
    set n n + 1
  end
end
