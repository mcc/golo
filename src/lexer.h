/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef GOLO_LEXER_H
#define GOLO_LEXER_H

#include "map.h"
#include "list.h"


/**
 * List of all possible tokens in the GOLO language.
 */
typedef enum golo_token_id_t
{
  GOLO_TK_NOT,        /**< Unary boolean negation operator */
  GOLO_TK_MUL,        /**< Multiplication operator */
  GOLO_TK_DIV,        /**< Division operator */
  GOLO_TK_MOD,        /**< Modulo operator */
  GOLO_TK_PLUS,       /**< Binary plus operator */
  GOLO_TK_MINUS,      /**< Binary minus operator */
  GOLO_TK_UPLUS,      /**< Unary plus operator */
  GOLO_TK_UMINUS,     /**< Unary minus operator */
  GOLO_TK_INF,        /**< Boolean < operator */
  GOLO_TK_INFEQ,      /**< Boolean <= operator */
  GOLO_TK_SUP,        /**< Boolean > operator */
  GOLO_TK_SUPEQ,      /**< Boolean >= operator */
  GOLO_TK_EQ,         /**< Boolean equality operator */
  GOLO_TK_NEQ,        /**< Boolean inequality operator */
  GOLO_TK_AND,        /**< Boolean and operator */
  GOLO_TK_OR,         /**< Boolean or operator */
  GOLO_TK_NUMBER,     /**< Floating point number */
  GOLO_TK_IDENTIFIER, /**< Identifier for variables and function names */
  GOLO_TK_MOVE,       /**< move keyword */
  GOLO_TK_TURN,       /**< turn keyword */
  GOLO_TK_GOTO,       /**< goto keyword */
  GOLO_TK_UP,         /**< up keyword */
  GOLO_TK_DOWN,       /**< down keyword */
  GOLO_TK_COLOR,      /**< color keyword */
  GOLO_TK_SET,        /**< set keyword */
  GOLO_TK_WHILE,      /**< while keyword */
  GOLO_TK_IF,         /**< if keyword */
  GOLO_TK_ELSE,       /**< else keyword */
  GOLO_TK_END,        /**< end keyword */
  GOLO_TK_LPAR,       /**< Left parenthesis */
  GOLO_TK_RPAR,       /**< Right parenthesis */
  GOLO_TK_FUNCTION,   /**< function keyword */
  GOLO_TK_RETURN,     /**< return keyword */
  GOLO_TK_BEGIN,      /**< begin keyword */
  GOLO_TK_BUILTIN,    /**< Built-in function (see golo_token_func_id_t) */
  GOLO_TK_EOL         /**< End of line or # */
} golo_token_id_t;


/**
 * List of built-in functions in the GOLO language.
 */
typedef enum golo_token_func_id_t
{
  GOLO_TK_FUNC_NONE,  /**< Not a function */
  GOLO_TK_FUNC_X,     /**< Current x position */
  GOLO_TK_FUNC_Y,     /**< Current y position */
  GOLO_TK_FUNC_R,     /**< Current red color component value */
  GOLO_TK_FUNC_G,     /**< Current green color component value */
  GOLO_TK_FUNC_B,     /**< Current blue color component value */
  GOLO_TK_FUNC_W,     /**< Width defined with the begin bloc */
  GOLO_TK_FUNC_H,     /**< Height defined with the begin bloc */
  GOLO_TK_FUNC_ANGLE, /**< Current angle */
  GOLO_TK_FUNC_COS,   /**< Cosine function */
  GOLO_TK_FUNC_SIN,   /**< Sine function */
  GOLO_TK_FUNC_TAN,   /**< Tangent function */
  GOLO_TK_FUNC_ACOS,  /**< Arc cosine function */
  GOLO_TK_FUNC_ASIN,  /**< Arc sine function */
  GOLO_TK_FUNC_ATAN,  /**< Arc tangent function */
  GOLO_TK_FUNC_ATAN2, /**< Arc tangent with two parameters function */
  GOLO_TK_FUNC_POW,   /**< Raise to power */
  GOLO_TK_FUNC_SQRT,  /**< Square root function */
  GOLO_TK_FUNC_LOG,   /**< Natural logarithm */
  GOLO_TK_FUNC_LOG10  /**< Common logarithm */
} golo_token_func_id_t;

/**
 * Represents a token.
 */
typedef struct golo_token_t
{
  golo_token_id_t id;          /**< Id (type) of the token */
  golo_token_func_id_t funcid; /**< Function id of the token (if any) */
  float           value;       /**< Float value for a number, 0 otherwise */
  char*           name;        /**< Name for an identifier, NULL otherwise */
  size_t          size;        /**< Number of characters of the token */
  size_t          arity;       /**< Arity for an operator, 0 otherwise */
  int             priority;    /**< Priority for an operator, -1 otherwise */
  size_t          line;        /**< Number of the line where appear the token */
  size_t          column;      /**< Column in the line where appear the token */
} golo_token_t;


/**
 * A lexer analyses a string to cut it into tokens.
 */
typedef struct golo_lexer_t
{
  map_t           tokens;             /**< Map of all predefined tokens */
  map_t           functions;          /**< Map of all built-in functions */
  golo_token_id_t last_read_token_id; /**< Token id read before "token" */
  golo_token_t    token;              /**< Last read token */
  char*           instr;              /**< Current instruction to tokenize */
  size_t          line;               /**< Current line number */
  size_t          column;             /**< Current column in the instruction */
} golo_lexer_t;


void golo_init_lexer(golo_lexer_t* lexer);
void golo_set_lexer_instr(golo_lexer_t* lexer, char* instr);
int  golo_is_token_inside_bloc(golo_token_id_t id);
int  golo_is_token_outside_bloc(golo_token_id_t id);
int  golo_token_is_value(golo_token_id_t id);
void golo_init_token(golo_lexer_t* lexer, golo_token_t* token);
int  golo_get_next_token(golo_lexer_t* lexer);
void golo_free_lexer(golo_lexer_t* lexer);

#endif
