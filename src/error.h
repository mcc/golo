/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#ifndef GOLO_ERROR_H
#define GOLO_ERROR_H


/**
 * List of all possible error codes.
 */
typedef enum golo_error_id_t
{
  GOLO_ERROR_SYNTAX,                    /**< Syntax error in an expression */
  GOLO_ERROR_MISSING_RPAR,              /**< Missing right parenthesis */
  GOLO_ERROR_MISSING_LPAR,              /**< Missing left parenthesis */
  GOLO_ERROR_MISSING_PARAM,             /**< Missing function parameter */
  GOLO_ERROR_MISSING_BEGIN,             /**< Missing BEGIN bloc */
  GOLO_ERROR_MISSING_BEGIN_END,         /**< Missing END for the BEGIN bloc */
  GOLO_ERROR_UNEXPECTED_END,            /**< Unexpected END keyword */
  GOLO_ERROR_UNEXPECTED_ELSE,           /**< Unexpected ELSE keyword */
  GOLO_ERROR_EXPECTED_POSITIVE_INTEGER, /**< A positive integer was expected */
  GOLO_ERROR_EXPECTED_IDENTIFIER,       /**< An identifier was expected */
  GOLO_ERROR_EXPECTED_EOL,              /**< The end of line was expected */
  GOLO_ERROR_EXPECTED_EXPR,             /**< An expression was expected */
  GOLO_ERROR_BEGIN_REDECLARATION,       /**< The begin bloc is redefined */
  GOLO_ERROR_INSTR_OUTSIDE_BLOC,        /**< An instruction appears outside a 
                                          * bloc */
  GOLO_ERROR_FUNC_INSIDE_BLOC,          /**< A function definition appears
                                          * inside a bloc */
  GOLO_ERROR_SET_FUNCTION,              /**< A SET instruction cannot modify abort
                                          * function */
  GOLO_ERROR_UNKNOWN_IDENTIFIER,        /**< Unknown identifier */
  GOLO_ERROR_UNKNOWN_FUNCTION,          /**< Unknown function */
  GOLO_ERROR_DIVISION_BY_ZERO           /**< Division by 0 */
} golo_error_id_t;


/**
 * Represents an error.
 */
typedef struct golo_error_t
{
  golo_error_id_t id;     /**< Id of the error */
  size_t          line;   /**< Line where appear the error */
  size_t          column; /**< Column in the line where appear the error */
} golo_error_t;


void         golo_error_set(golo_error_id_t id, size_t line, size_t column);
golo_error_t golo_error_get();
void         golo_error_print();


#endif
