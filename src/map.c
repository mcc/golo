/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file map.c
 * 
 * This file provides functions to associate a string to a value. Each string
 * added in the map is represented as a path in a tree where each node contains
 * a letter and a value (if the node corresponds to the end of a string).
 */


#include <stdlib.h>
#include <stdio.h>
#include "map.h"


/**
 * Initializes a map.
 *
 * @param map map instance to initialize
 */
void map_init(map_t* map)
{
  map->next = NULL;
  map->child = NULL;
  map->data = NULL;
}


/**
 * Associates a value to the specified string. If size != -1, we use only the
 * size first characters of the string. Otherwise, we assume that str is
 * null-terminated and we use the whole string.
 *
 * @param map map instance to use
 * @param str string to use
 * @param size number of characters to use in str
 * @param data data to assign to the given string
 * @returns the old value if any or NULL otherwise
 */
void* map_set(map_t* map, const char* str, int size, void* data)
{
  map_t* child = NULL;
  void* old_data = NULL;
  
  if (size != 0)
  {
    for (child = map->child;
         child != NULL && child->label != str[0];
         child = child->next);
         
    if (child == NULL)
    {
      child = malloc(sizeof(map_t));
      child->child = NULL;
      child->data = NULL;
      child->next = map->child;
      map->child = child;
    }
    child->label = str[0];
    
    if (str[1] != '\0' && (size > 1 || size < 0))
    {
      old_data = map_set(child, str + 1, size - 1, data);
    }
    else
    {
      old_data = child->data;
      child->data = data;
    }
  }
  
  return old_data;
}


/**
 * Gets the value associated with the specified string str if any or NULL
 * otherwise. If size != -1, we check only the size first characters of the
 * string. Otherwise, we assume that str is null-terminated and we check the
 * whole string. read_size is modified by the number of characters read in str.
 * If a non NULL pointer is returned but read_size is different than the
 * expected size (size if size != -1 or strlen(str) otherwhise), the pointer
 * corresponds to the value associated to the left substring of size read_size.
 *
 * @param map map instance to use
 * @param str string to search
 * @param size number of characters to read in str
 * @param read_size pointer modified by the number of read characters in str
 * @returns the value associated to the left substring of str of size read_size
 */
void* map_get(map_t* map, const char* str, int size, size_t* read_size)
{
  map_t* child = NULL;
  void* data = map->data;
  
  if (size != 0)
  {
    for (child = map->child;
         child != NULL && child->label != str[0];
         child = child->next);
         
    if (child != NULL)
    {
      if (read_size != NULL)
      {
        (*read_size)++;
      }
      if (str[1] != '\0' && (size > 1 || size < 0))
      {
        data = map_get(child, str + 1, size - 1, read_size);
      }
      else
      {
        data = child->data;
      }
    }
  }
  
  return data;
}


/**
 * Frees the map.
 *
 * @param map map instance to free
 * @param free_data if its value is 1, elements data are freed, otherwise only
 *        nodes are freed
 */
void map_free(map_t* map, int free_data)
{
  map_t* tmp;
  map_t* child = map->child;
  while (child != NULL)
  {
    tmp = child;
    child = tmp->next;
    if (tmp->data != NULL && free_data)
    {
      free(tmp->data);
      tmp->data = NULL;
    }
    map_free(tmp, free_data);
    free(tmp);
  }
  map->child = NULL;
}
