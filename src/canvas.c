/*
 * Copyright (c) 2016, Maxime BRIDE
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


/**
 * @file canvas.c
 * 
 * This file provides functions to draw on a canvas. It contains in particular
 * a simple implementation of the Bresenham's line algorithm.
 */
 
 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "canvas.h"


#define INSIDE 0 // 0000
#define LEFT   1 // 0001
#define RIGHT  2 // 0010
#define BOTTOM 4 // 0100
#define TOP    8 // 1000

#define M_PI 3.14159265358979323846

#define BMP_HEADER_SIZE 54

#define UINT(value) (value) &               0x000000FF,\
                   ((value) &               0x0000FF00) >> 8,\
                   ((value) &               0x00FF0000) >> 16,\
                   ((value) & (unsigned int)0xFF000000) >> 24


/**
 * Initialize a canvas. Must be called before any other function on it.
 *
 * @param canvas the canvas to initialize
 * @param width width for the canvas in pixels
 * @param height height for the canvas in pixels
 */
void canvas_init(canvas_t* canvas, int width, int height)
{
  int m, n;
  
  canvas->width = width;
  canvas->height = height;
  canvas->x = width / 2;
  canvas->y = height / 2;
  canvas->angle = 0;
  canvas->isdown = 1;
  canvas->color.r = 0;
  canvas->color.g = 0;
  canvas->color.b = 0;
  
  if (width <= 0 || height <= 0)
  {
    canvas->pixels = NULL;
  }
  else
  {
    canvas->pixels = malloc(height * sizeof(color_t*));
    for (m = 0; m < height; m++)
    {
      canvas->pixels[m] = malloc(width * sizeof(color_t));
      for (n = 0; n < width; n++)
      {
        canvas->pixels[m][n].r = 255;
        canvas->pixels[m][n].g = 255;
        canvas->pixels[m][n].b = 255;
      }
    }
  }
}


/**
 * Converts a coordinate (x,y) from (resp. to) the octant 0 to (resp. from) the
 * given octant. The result is stored in (nx,ny).
 *
 * @param octant the octant from (or to) which we do the conversion
 * @param x original x position
 * @param y original y position
 * @param nx converted x position
 * @param ny converted y position
 * @param from_zero if 0 we convert from the given octant to the octant zero,
 *                  if 1 we convert from octant zero to the given octant
 */
static void canvas_octant_convert(int octant, int x, int y, int* nx, int* ny,
                                  int from_zero)
{
  switch (octant)
  {
    default:
    case 0:
      *nx = x;
      *ny = y;
      break;
    case 1:
      *nx = y;
      *ny = x;
      break;
    case 2:
      *nx = from_zero ? -y : y;
      *ny = from_zero ? x : -x;
      break;
    case 3:
      *nx = -x;
      *ny = y;
      break;
    case 4:
      *nx = -x;
      *ny = -y;
      break;
    case 5:
      *nx = -y;
      *ny = -x;
      break;
    case 6:
      *nx = from_zero ? y : -y;
      *ny = from_zero ? -x : x;
      break;
    case 7:
      *nx = x;
      *ny = -y;
      break;
  }
}


/**
 * Get the octant where the line (0,0) -> (x,y) is located.
 *
 * @param x line end x position
 * @param y line end y position
 * @returns the octant where the line (0,0) -> (x,y) is located
 */
static int canvas_octant(int x, int y)
{
  /* \5|6/ 
     4\|/7 
    ---+---
     3/|\0 
     /2|1\ */
       
  int d = abs(x) > abs(y);
  int octant = 0;
  
  if (x > 0)
  {
    if (y > 0)
    {
      octant = d ? 0 : 1;
    }
    else
    {
      octant = d ? 7 : 6;
    }
  }
  else
  {
    if (y > 0)
    {
      octant = d ? 3 : 2;
    }
    else
    {
      octant = d ? 4 : 5;
    }
  }
  
  return octant;
}


/**
 * Draw a pixel on the canvas with the specified color.
 *
 * @param canvas canvas on which draw the pixel
 * @param x x position of the pixel
 * @param y y position of the pixel
 * @param color color of the pixel
 */
static void canvas_plot(canvas_t* canvas, int x, int y, color_t color)
{  
  if (x >= 0 && y >= 0 && y < canvas->height && x < canvas->width)
  {
    canvas->pixels[y][x] = color;
  }
}


/**
 * Draw the line (x1,y1) -> (x2,y2) on the given canvas. We use the 
 * Bresenham's line algorithm.
 *
 * @param canvas canvas on which draw the line
 * @param x1 start x position of the line
 * @param y1 start y position of the line
 * @param x2 end x position of the line
 * @param y2 end y position of the line
 */
void canvas_draw_line(canvas_t* canvas, int x1, int y1, int x2, int y2)
{
  /*
    Source : https://en.wikipedia.org/wiki/Bresenham's_line_algorithm
    We work in the octant 0 considering that the line starts at (0,0).
    We make the conversion from octant zero to the target octant and we add
    x1 and y1 to obtain the desired coordinate.
   */
       
  int dx, dy, dx2, dy2, d, x, y, nx, ny, octant;
  
  y = 0;
  x2 -= x1;
  y2 -= y1;
  octant = canvas_octant(x2, y2);
  canvas_octant_convert(octant, x2, y2, &dx, &dy, 0);
  d = -dx;
  dx2 = 2 * dx;
  dy2 = 2 * dy;
    
  for (x = 0; x <= dx; x++)
  {
    canvas_octant_convert(octant, x, y, &nx, &ny, 1);
    canvas_plot(canvas, nx + x1, ny + y1, canvas->color);
    d += dy2;
    if (d > 0)
    {
      y++;
      d -= dx2;
    }
  }
}


/**
 * Performs a rotation of the pen. The function has to be used with canvas_move.
 *
 * @param canvas canvas to modify
 * @param angle angle of rotation in degree
 */
void canvas_turn(canvas_t* canvas, double angle)
{
  canvas->angle = fmod(canvas->angle + angle, 360);
}


/**
 * Moves by the specified distance based on the current angle and position.
 *
 * @param canvas canvas to modify
 * @param distance distance to cover
 */
void canvas_move(canvas_t* canvas, double distance)
{
  double rad_angle = canvas->angle * M_PI / 180;
  canvas_goto(canvas, canvas->x + distance * cos(rad_angle),
                      canvas->y + distance * sin(rad_angle));
}


/**
 * Compute the bit code for a point (x,y) using the rectangle defined by the
 * canvas.
 *
 * @param canvas canvas to use
 * @param x x coordinate of the point
 * @param y y coordinate of the point
 * @returns the bit code of the specified point 
 */
static int canvas_compute_outcode(canvas_t* canvas, double x, double y)
{
  int code = INSIDE;

  if (x < 0)
  {
    code |= LEFT;
  }
  else if (x > canvas->width)
  {
    code |= RIGHT;
  }
  
  if (y < 0)
  {
    code |= BOTTOM;
  }
  else if (y > canvas->height)
  {
    code |= TOP;
  }

  return code;
}


/**
 * Clips a line from (x1,y1) to (x2,y2) against the rectangle defined by the
 * canvas by using the Cohen–Sutherland clipping algorithm. The function
 * modifies x1, y1, x2 and y2 with values corresponding to the coordinates of
 * the clipped line.
 *
 * @param canvas canvas to use
 * @param x1 starting x coordinate of the line to clip
 * @param y1 starting y coordinate of the line to clip
 * @param x2 ending x coordinate of the line to clip
 * @param y2 ending y coordinate of the line to clip
 * @returns 1 if the line intersects with the canvas, 0 otherwise
 */
static int canvas_clip_line(canvas_t* canvas,
                            double* x1, double* y1,
                            double* x2, double* y2)
{
  int outcode1 = canvas_compute_outcode(canvas, *x1, *y1);
  int outcode2 = canvas_compute_outcode(canvas, *x2, *y2);
  int outcode;
  int accept = 0;
  int done = 0;
  double x = 0;
  double y = 0;

  while (!done) {
    if (!(outcode1 | outcode2))
    {
      accept = 1;
      done = 1;
    }
    else if (outcode1 & outcode2)
    {
      done = 1;
    }
    else
    {
      outcode = outcode1 ? outcode1 : outcode2;
      
      if ((outcode & TOP) || (outcode & BOTTOM))
      {
        y = (outcode & TOP) ? canvas->height : 0;
        x = *x1 + (*x2 - *x1) * (y - *y1) / (*y2 - *y1);
      }
      else if ((outcode & RIGHT) || (outcode & LEFT))
      {
        x = (outcode & RIGHT) ? canvas->width : 0;
        y = *y1 + (*y2 - *y1) * (x - *x1) / (*x2 - *x1);
      }

      if (outcode == outcode1) {
        *x1 = x;
        *y1 = y;
        outcode1 = canvas_compute_outcode(canvas, *x1, *y1);
      } else {
        *x2 = x;
        *y2 = y;
        outcode2 = canvas_compute_outcode(canvas, *x2, *y2);
      }
    }
  }
  
  return accept;
}


/**
 * Sets the position to the specified one and draw the line segment between the
 * current and the new position which intersects with the canvas.
 *
 * @param canvas canvas to modify
 * @param x new x position
 * @param y new y position
 */
void canvas_goto(canvas_t* canvas, double x, double y)
{
  double x1 = canvas->x;
  double y1 = canvas->y;
  double x2 = x;
  double y2 = y;
  
  if (canvas->isdown && canvas_clip_line(canvas, &x1, &y1, &x2, &y2))
  {
    canvas_draw_line(canvas, round(x1), round(y1), round(x2), round(y2));
  }
  canvas->x = x;
  canvas->y = y;
}


/**
 * Generate a BMP file from the canvas.
 *
 * @param canvas canvas to export in BMP format
 * @param output output to be used for data
 */
void canvas_export(canvas_t* canvas, FILE* output)
{
  int padding    = (4 - ((canvas->width * 3) % 4)) % 4;
  int paddedsize = ((canvas->width * 3) + padding) * canvas->height;
  int x;
  int y;
  int index;
  unsigned char* buffer;
  unsigned char header[BMP_HEADER_SIZE] =
  {
    'B', 'M',
    UINT(paddedsize + BMP_HEADER_SIZE), // bfSize (whole file size)
    UINT(0),                            // bfReserved
    UINT(BMP_HEADER_SIZE),              // bfOffbits
    UINT(BMP_HEADER_SIZE - 14),         // biSize
    UINT(canvas->width),                // biWidth
    UINT(canvas->height),               // biHeight
    1, 0, 24, 0,                        // biPlanes, biBitCount
    UINT(0),                            // biCompression
    UINT(paddedsize),                   // biSizeImage
    UINT(0),                            // biXPelsPerMeter
    UINT(0),                            // biYPelsPerMeter
    UINT(0),                            // biClrUsed
    UINT(0)                             // biClrImportant
  };

  // BMP image format is written from bottom to top.
  buffer = malloc(paddedsize);
  memset(buffer, 0, paddedsize);
  for (y = 0; y < canvas->height; y++)
  {
    for (x = 0; x < canvas->width; x++)
    {
      index = 3 * (y * canvas->width + x);
      buffer[index]     = canvas->pixels[canvas->height - y - 1][x].b;
      buffer[index + 1] = canvas->pixels[canvas->height - y - 1][x].g;
      buffer[index + 2] = canvas->pixels[canvas->height - y - 1][x].r;
    }
  }

  fwrite(header, 1, BMP_HEADER_SIZE, output);
  fwrite(buffer, 1, paddedsize, output);
  free(buffer);
}


/**
 * Frees the memory occupied by the canvas.
 *
 * @param canvas canvas to free
 */
void canvas_free(canvas_t* canvas)
{
  int n;
  
  if (canvas->pixels != NULL)
  {
    for (n = 0; n < canvas->height; n++)
    {
      free(canvas->pixels[n]);
    }
    free(canvas->pixels);
    canvas->pixels = NULL;
  }
}
