cmake_minimum_required(VERSION 2.8)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -O3")

include_directories(src)

project(golo)

set(SRCS
  src/canvas.c
  src/error.c
  src/interpreter.c
  src/lexer.c
  src/list.c
  src/map.c
  src/parser.c
)
    
set(HEADERS
  src/canvas.h
  src/error.h
  src/interpreter.h
  src/lexer.h
  src/list.h
  src/map.h
  src/parser.h
)

set(SRCS_TESTS
  tests/golo_tests.c
  tests/test_canvas.c
  tests/test_interpreter.c
  tests/test_lexer.c
  tests/test_list.c
  tests/test_map.c
  tests/test_parser.c
)

set(HEADERS_TESTS
  tests/assert.h
  tests/test_canvas.h
  tests/test_interpreter.h
  tests/test_lexer.h
  tests/test_list.h
  tests/test_map.h
  tests/test_parser.h
)

add_executable(golo src/main.c ${SRCS} ${HEADERS})
add_executable(golo_tests ${SRCS_TESTS} ${HEADERS_TESTS} ${SRCS} ${HEADERS})

target_link_libraries(golo m)
target_link_libraries(golo_tests m)
